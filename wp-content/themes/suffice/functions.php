<?php
/**
 * Suffice functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ThemeGrill
 * @subpackage Suffice
 * @since Suffice 1.0.0
 */

if (!function_exists('suffice_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function suffice_setup()
    {

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on suffice, use a find and replace
         * to change 'suffice' to the name of your theme in all the template files.
         */
        load_theme_textdomain('suffice', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        /**
         * Image sizes
         *
         * Make sure height and width are double to make sure it comes in srcset
         */
        add_image_size('suffice-thumbnail-grid', 750, 420, true);

        // Image size for featured posts.
        add_image_size('suffice-thumbnail-featured-one', 295, 525, true);
        add_image_size('suffice-thumbnail-featured-two', 1140, 504, true);

        // Image size for portfolio.
        add_image_size('suffice-thumbnail-portfolio', 572, 552, true);
        add_image_size('suffice-thumbnail-portfolio-masonry', 572, 652, true);

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'suffice'),
            'social' => esc_html__('Social', 'suffice'),
            'footer' => esc_html__('Footer', 'suffice'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Add theme support for woocommerce.
        add_theme_support('woocommerce');

        // Add theme support for woocommerce product gallery added in WooCommerce 3.0.
        add_theme_support('wc-product-gallery-lightbox');
        add_theme_support('wc-product-gallery-zoom');
        add_theme_support('wc-product-gallery-slider');

        // Add theme support for custom logo.
        add_theme_support('custom-logo');

        // Add theme support for SiteOrigin Page Builder.
        add_theme_support('siteorigin-panels', array(
            'margin-bottom' => 30,
            'recommended-widgets' => false,
        ));

        /*
        * This theme styles the visual editor to resemble the theme style,
        * specifically font, colors, and column width.
        */
        add_editor_style(array('editor-style.css', suffice_fonts_url()));

    }
endif;
add_action('after_setup_theme', 'suffice_setup');

if (!function_exists('suffice_content_width')) :

    /**
     * Set the content width in pixels, based on the theme's design and stylesheet.
     *
     * Priority 0 to make it available to lower priority callbacks.
     *
     * @global int $content_width
     */
    function suffice_content_width()
    {
        $content_width = 760;
        if ('full-width' === suffice_get_current_layout()) {
            $content_width = 1140;
        }

        $GLOBALS['content_width'] = apply_filters('suffice_content_width', $content_width);
    }
endif;
add_action('template_redirect', 'suffice_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function suffice_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar Left', 'suffice'),
        'id' => 'sidebar-left',
        'description' => esc_html__('Add widgets here.', 'suffice'),
        'before_widget' => '<section id="%1$s" class="widget ' . suffice_get_widget_class() . ' %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Sidebar Right', 'suffice'),
        'id' => 'sidebar-right',
        'description' => esc_html__('Add widgets here.', 'suffice'),
        'before_widget' => '<section id="%1$s" class="widget ' . suffice_get_widget_class() . ' %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Sidebar 1', 'suffice'),
        'id' => 'footer-sidebar-1',
        'description' => esc_html__('Add widgets here.', 'suffice'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Sidebar 2', 'suffice'),
        'id' => 'footer-sidebar-2',
        'description' => esc_html__('Add widgets here.', 'suffice'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Sidebar 3', 'suffice'),
        'id' => 'footer-sidebar-3',
        'description' => esc_html__('Add widgets here.', 'suffice'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Sidebar 4', 'suffice'),
        'id' => 'footer-sidebar-4',
        'description' => esc_html__('Add widgets here.', 'suffice'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'suffice_widgets_init');

/**
 * Registers google fonts.
 */
if (!function_exists('suffice_fonts_url')) :
    function suffice_fonts_url()
    {
        $fonts_url = '';
        $fonts = array();
        $subsets = 'latin,latin-ext';

        /**
         * Translators: If there are characters in your language that are not
         * supported by Open Sans, translate this to 'off'. Do not translate
         * into your own language.
         */
        if ('off' !== _x('on', 'Open Sans font: on or off', 'suffice')) {
            $fonts[] = 'Open Sans:400,400i,700,700i';
        }

        /**
         * Translators: If there are characters in your language that are not
         * supported by Poppins, translate this to 'off'. Do not translate
         * into your own language.
         */
        if ('off' !== _x('on', 'Poppins font: on or off', 'suffice')) {
            $fonts[] = 'Poppins:400,500,600,700';
        }

        if ($fonts) {
            $fonts_url = add_query_arg(array(
                'family' => urlencode(implode('|', $fonts)),
                'subset' => urlencode($subsets),
            ), 'https://fonts.googleapis.com/css');
        }

        return $fonts_url;
    }
endif;


/**
 * Enqueue scripts and styles.
 */
function suffice_scripts()
{
    $suffix = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

    /* Google fonts */
    wp_enqueue_style('suffice-fonts', suffice_fonts_url(), array(), null);

    /* Stylesheets */
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome' . $suffix . '.css', array(), '4.7');
    wp_enqueue_style('swiper', get_template_directory_uri() . '/assets/css/swiper' . $suffix . '.css', array(), '3.4.0');
    wp_enqueue_style('perfect-scrollbar', get_template_directory_uri() . '/assets/css/perfect-scrollbar' . $suffix . '.css', array(), '0.6.16');
    wp_enqueue_style('suffice-style', get_stylesheet_uri());

    /* Scripts */
    wp_enqueue_script('suffice-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true);
    wp_enqueue_script('swiper', get_template_directory_uri() . '/assets/js/swiper.jquery' . $suffix . '.js', array('jquery'), '3.4.0', true);
    wp_enqueue_script('waypoints', get_template_directory_uri() . '/assets/js/jquery.waypoints' . $suffix . '.js', array('jquery'), '4.0.1', true);
    wp_enqueue_script('visible', get_template_directory_uri() . '/assets/js/jquery.visible' . $suffix . '.js', array('jquery'), '1.0.0', true);
    if (true == suffice_get_option('suffice_sticky_header', true)) {
        wp_enqueue_script('headroom', get_template_directory_uri() . '/assets/js/headroom' . $suffix . '.js', array('jquery'), '0.9', true);
        wp_enqueue_script('headroom-jquery', get_template_directory_uri() . '/assets/js/jQuery.headroom' . $suffix . '.js', array('jquery'), '0.9', true);
    }
    wp_enqueue_script('perfect-scrollbar', get_template_directory_uri() . '/assets/js/perfect-scrollbar.jquery' . $suffix . '.js', array('jquery'), '0.6.16', true);
    wp_enqueue_script('isotope', get_template_directory_uri() . '/assets/js/isotope.pkgd' . $suffix . '.js', array('jquery'), '3.0.2', true);
    wp_enqueue_script('countup', get_template_directory_uri() . '/assets/js/countUp' . $suffix . '.js', array('jquery'), '1.8.3', true);
    wp_enqueue_script('smooth-scroll', get_template_directory_uri() . '/assets/js/smooth-scroll' . $suffix . '.js', array('jquery'), '10.2.1', true);
    wp_enqueue_script('gumshoe', get_template_directory_uri() . '/assets/js/gumshoe' . $suffix . '.js', array('jquery'), '3.3.3', true);
    /* Loads sticky sidebar js if enabled */
    if (true == suffice_get_option('suffice_sticky_sidebar', false)) {
        wp_enqueue_script('theia-sticky-sidebar', get_template_directory_uri() . '/assets/js/theia-sticky-sidebar' . $suffix . '.js', array('jquery'), false, true);
        wp_enqueue_script('ResizeSensor', get_template_directory_uri() . '/assets/js/ResizeSensor' . $suffix . '.js', array('jquery'), false, true);
    }
    // Include Google Maps Js.
    $googlemapapi = suffice_get_option('suffice_google_map_api', '');
    if (!empty($googlemapapi)) {
        wp_register_script('googlemap', 'https://maps.googleapis.com/maps/api/js?key=' . $googlemapapi . '', false, '3.0.0', true);
    }
    wp_enqueue_script('suffice-custom', get_template_directory_uri() . '/assets/js/suffice-custom' . $suffix . '.js', array('jquery'), '1.0', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'suffice_scripts');

/**
 * Adds css style for customizer ui
 */
function suffice_customizer_controls_styles()
{
    $theme_version = wp_get_theme()->get('version');
    wp_enqueue_style('customizer', get_template_directory_uri() . '/assets/css/customizer.css', array(), $theme_version);
}

add_action('customize_controls_enqueue_scripts', 'suffice_customizer_controls_styles');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * CSS class handling function.
 */
require get_template_directory() . '/inc/template-css-class.php';

/**
 * Custom template functions.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * TGM Plugin Activation.
 */
require get_template_directory() . '/inc/class-tgm-plugin-activation.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom meta boxes
 */
if (true == suffice_get_option('suffice_sticky_header', true)) {
    require get_template_directory() . '/inc/meta-boxes.php';
}

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * If woocommerce is active, load compatibility file
 */
if (suffice_is_woocommerce_active()) {
    require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Kirki Customizer
 */
require get_template_directory() . '/inc/kirki/kirki.php';
require get_template_directory() . '/inc/kirki-translation.php';
require get_template_directory() . '/inc/kirki-customizer.php';

/**
 * Load Demo Importer Configs.
 */
if (class_exists('TG_Demo_Importer')) {
    require get_template_directory() . '/inc/demo-config.php';
}

/**
 * Assign the Suffice version to a variable.
 */
$theme = wp_get_theme('suffice');
$suffice_version = $theme['Version'];

/* Calling in the admin area for the Welcome Page */
if (is_admin()) {
    require get_template_directory() . '/inc/admin/class-suffice-admin.php';
}

/** Freemius */
// Create a helper function for easy SDK access.
function suffice_fs()
{
    global $suffice_fs;

    if (!isset($suffice_fs)) {
        // Include Freemius SDK.
        require_once dirname(__FILE__) . '/freemius/start.php';

        $suffice_fs = fs_dynamic_init(array(
            'id' => '1217',
            'slug' => 'suffice',
            'type' => 'theme',
            'public_key' => 'pk_085bdc87271236b93bd78b164b768',
            'is_premium' => false,
            'has_addons' => false,
            'has_paid_plans' => false,
            'menu' => array(
                'slug' => 'suffice-welcome',
                'account' => false,
                'support' => false,
                'parent' => array(
                    'slug' => 'themes.php',
                ),
            ),
        ));
    }

    return $suffice_fs;
}

// Init Freemius.
suffice_fs();
// Signal that SDK was initiated.
do_action('suffice_fs_loaded');


function add_e2_date_picker()
{
    //jQuery UI date picker file
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('my_custom_script', get_bloginfo('template_url') . '/js/custom.js', array('jquery'));
    wp_enqueue_style('e2b-admin-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css', false, "1.9.0", false);
    wp_enqueue_style('custom-css', get_bloginfo('template_url') . '/css/custom.css');
}

add_action('admin_enqueue_scripts', 'add_e2_date_picker');

/*Creating a function to create our event CPT*/

function wpt_event_post_type()
{
    $labels = array(
        'name' => __('Events'),
        'singular_name' => __('Event'),
        'add_new' => __('Add New Event'),
        'add_new_item' => __('Add New Event'),
        'edit_item' => __('Edit Event'),
        'new_item' => __('Add New Event'),
        'view_item' => __('View Event'),
        'search_items' => __('Search Event'),
        'not_found' => __('No events found'),
        'not_found_in_trash' => __('No events found in trash')
    );
    $supports = array(
        'title',
        'editor',
        'thumbnail'
    );
    $args = array(
        'labels' => $labels,
        'supports' => $supports,
        'public' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'events'),
        'has_archive' => true,
        'menu_position' => 30,
        'menu_icon' => 'dashicons-calendar-alt',
        'register_meta_box_cb' => 'wpt_add_event_metaboxes',
    );

    register_post_type('events', $args);
}

add_action('init', 'wpt_event_post_type');

/*Adding metaboxex for event post type.*/

add_action('add_meta_boxes', 'add_events_metaboxes');

function add_events_metaboxes()
{
    add_meta_box(
        'wpt_start_date',
        'Start Date',
        'wpt_start_date',
        'events',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_end_date',
        'End Date',
        'wpt_end_date',
        'events',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_time',
        'Time',
        'wpt_time',
        'events',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_location',
        'Location',
        'wpt_location',
        'events',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_event_code',
        'Event Code',
        'wpt_event_code',
        'events',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_prize',
        'Prize',
        'wpt_prize',
        'events',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_organizer_details',
        'Organizer details',
        'wpt_organizer_details',
        'events',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_email',
        'Email',
        'wpt_email',
        'events',
        'normal',
        'default'
    );
}

function wpt_start_date()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'start_date');
    // Get the location data if it's already been entered
    $start_date = get_post_meta($post->ID, 'event_start_date', true);
    // Output the field
    echo '<input type="text" name="event_start_date" value="' . esc_textarea($start_date) . '" class="datepicker common_width">';
}

function wpt_end_date()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'end_date');
    // Get the location data if it's already been entered
    $end_date = get_post_meta($post->ID, 'event_end_date', true);
    // Output the field
    echo '<input type="text" name="event_end_date" value="' . esc_textarea($end_date) . '" class="datepicker common_width">';
}

function wpt_time()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'time');
    // Get the location data if it's already been entered
    $time = get_post_meta($post->ID, 'event_time', true);
    // Output the field
    echo '<input type="text" name="event_time" value="' . esc_textarea($time) . '" class="datepicker common_width">';
}

function wpt_location()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'location');
    // Get the location data if it's already been entered
    $location = get_post_meta($post->ID, 'event_location', true);
    // Output the field
    /*    echo '<textarea name="event_location"  class="datepicker"> <?php echo $event_location ?> </textarea>';*/
    $out = '<textarea class="common_width" name="event_location">' . $location . '</textarea>';
    echo $out;
}

function wpt_event_code()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'event_code');
    // Get the location data if it's already been entered
    $event_code = get_post_meta($post->ID, 'event_event_code', true);
    // Output the field
    echo '<input type="text" name="event_event_code" value="' . esc_textarea($event_code) . '" class="common_width">';
}

function wpt_prize()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'event_prize');
    // Get the location data if it's already been entered
    $event_prize = get_post_meta($post->ID, 'event_prize', true);
    // Output the field
    echo '<input type="text" name="event_prize" value="' . esc_textarea($event_prize) . '" class="common_width">';
}

function wpt_organizer_details()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'organizer_details');
    // Get the location data if it's already been entered
    $organizer_details = get_post_meta($post->ID, 'event_organizer_details', true);
    // Output the field
    /*    echo '<textarea name="event_location"  class="datepicker"> <?php echo $event_location ?> </textarea>';*/
    $out = '<textarea class="common_width" name="event_organizer_details" >' . $organizer_details . '</textarea>';
    echo $out;
}

function wpt_email()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'email');
    // Get the location data if it's already been entered
    $location_date = get_post_meta($post->ID, 'event_email', true);
    // Output the field
    /*    echo '<textarea name="event_location"  class="datepicker"> <?php echo $event_location ?> </textarea>';*/
    $out = '<textarea class="common_width" name="event_email">' . $location_date . '</textarea>';
    echo $out;
}

/**
 * Save the events metabox data
 */
function wpt_save_events_meta($post_id, $post)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
    if ($parent_id = wp_is_post_revision($post_id)) {
        $post_id = $parent_id;
    }

    // Now that we're authenticated, time to save the data.
    // This sanitizes the data from the field and saves it into an array $events_meta.

    $events_meta['event_start_date'] = esc_textarea($_POST['event_start_date']);
    $events_meta['event_end_date'] = esc_textarea($_POST['event_end_date']);
    $events_meta['event_time'] = esc_textarea($_POST['event_time']);
    $events_meta['event_location'] = esc_textarea($_POST['event_location']);
    $events_meta['event_event_code'] = esc_textarea($_POST['event_event_code']);
    $events_meta['event_prize'] = esc_textarea($_POST['event_prize']);
    $events_meta['event_organizer_details'] = esc_textarea($_POST['event_organizer_details']);
    $events_meta['event_email'] = esc_textarea($_POST['event_email']);
    $events_meta['event_name'] = esc_textarea($_POST['event_name']);


    // Cycle through the $events_meta array.
    // Note, in this example we just have one item, but this is helpful if you have multiple.
    foreach ($events_meta as $key => $value) :
        // Don't store custom data twice
        if ('revision' === $post->post_type) {
            return;
        }
        if (get_post_meta($post_id, $key, false)) {
            // If the custom field already has a value, update it.
            update_post_meta($post_id, $key, $value);
        } else {
            // If the custom field doesn't have a value, add it.
            add_post_meta($post_id, $key, $value);
        }
        if (!$value) {
            // Delete the meta key if there's no value
            delete_post_meta($post_id, $key);
        }
    endforeach;
}

add_action('save_post', 'wpt_save_events_meta', 1);


/*
* Our player custom post type function
*/

function wpt_player_post_type()
{
    $labels = array(
        'name' => __('Players'),
        'singular_name' => __('Player'),
        'add_new' => __('Add New Player'),
        'add_new_item' => __('Add New Player'),
        'edit_item' => __('Edit Player'),
        'new_item' => __('Add New Player'),
        'view_item' => __('View Player'),
        'search_items' => __('Search Player'),
        'not_found' => __('No Player found'),
        'not_found_in_trash' => __('No Player found in trash')
    );
    $supports = array(
        'title',
        'editor',
    );
    $args = array(
        'labels' => $labels,
        'supports' => $supports,
        'public' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'players'),
        'has_archive' => true,
        'menu_position' => 40,
        'menu_icon' => 'dashicons-admin-post',
        'register_meta_box_cb' => 'wpt_add_player_metaboxes',
    );

    register_post_type('players', $args);
}

add_action('init', 'wpt_player_post_type');

add_action('init', function () {
    remove_post_type_support('players', 'editor');
}, 99);

add_action('add_meta_boxes', 'add_players_metaboxes');


function add_players_metaboxes()
{
    add_meta_box(
        'wpt_name',
        'Name',
        'wpt_name',
        'players',
        'normal',
        'default'
    );

    add_meta_box(
        'wpt_title',
        'Title',
        'wpt_title',
        'players',
        'normal',
        'default'
    );

    add_meta_box(
        'wpt_state',
        'State',
        'wpt_state',
        'players',
        'normal',
        'default'
    );

    add_meta_box(
        'wpt_ratings',
        'Ratings',
        'wpt_ratings',
        'players',
        'normal',
        'default'
    );

    add_meta_box(
        'wpt_regstatus',
        'Regstatus',
        'wpt_regstatus',
        'players',
        'normal',
        'default'
    );
    add_meta_box(
        'wpt_gender',
        'Gender',
        'wpt_gender',
        'players',
        'normal',
        'default'
    );
}

function wpt_name()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'name');
    // Get the location data if it's already been entered
    $player_name = get_post_meta($post->ID, 'player_name', true);
    // Output the field
    echo '<input type="text" name="player_name" value="' . esc_textarea($player_name) . '" class="common_width">';
}

function wpt_title()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'title');
    // Get the location data if it's already been entered
    $player_title = get_post_meta($post->ID, 'player_title', true);
    // Output the field
    echo '<input type="text" name="player_title" value="' . esc_textarea($player_title) . '" class="common_width">';
}

function wpt_state()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'state');
    // Get the location data if it's already been entered
    $player_state = get_post_meta($post->ID, 'player_state', true);
    // Output the field
    echo '<select  name="player_state" value="' . esc_textarea($player_state) . '" class="common_width"
                                <option>Select a State</option>                        
                                <option value="Delhi">Delhi</option>
                                <option value="Tamil Nadu">Tamil Nadu</option>
                                <option value="Andhra Pradesh">Andhra Pradesh</option>
                                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                <option value="Assam">Assam</option>
                                <option value="Bihar">Bihar</option>
                                <option value="Chhattisgarh">Chhattisgarh</option>
                                <option value="Goa">Goa</option>
                                <option value="Gujarat">Gujarat</option>
                                <option value="1Himachal Pradesh">Himachal Pradesh</option>
                                <option value="Haryana">Haryana</option>
                                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                <option value="Jharkhand">Jharkhand</option>
                                <option value="Karnataka">Karnataka</option>
                                <option value="Kerala">Kerala</option>
                                <option value="Madhya Pradesh">Madhya Pradesh</option>
                                <option value="Maharashtra">Maharashtra</option>
                                <option value="Manipur">Manipur</option>
                                <option value="Meghalaya">Meghalaya</option>
                                <option value="Mizoram">Mizoram</option>
                                <option value="Orissa">Orissa</option>
                                <option value="Nagaland">Nagaland</option>
                                <option value="Punjab">Punjab</option>
                                <option value="Rajasthan">Rajasthan</option>
                                <option value="Sikkim">Sikkim</option>
                                <option value="Tripura">Tripura</option>
                                <option value="Uttrakhand">Uttrakhand</option>
                                <option value="Uttar Pradesh">Uttar Pradesh</option>
                                <option value="West Bengal">West Bengal</option>
                                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                <option value="Chandigarh">Chandigarh</option>
                                <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
                                <option value="Lakshadeep">Lakshadeep</option>
                                <option value="Pondicherry">Pondicherry</option>
                                <option value="Daman and Diu">Daman and Diu</option>
                                <option value="Telangana">Telangana</option>
                                <option value="Others">Others</option>
                            </select>';
}


function wpt_ratings()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'ratings');
    // Get the location data if it's already been entered
    $player_ratings = get_post_meta($post->ID, 'player_ratings', true);
    // Output the field
    echo '<input type="text" name="player_ratings" value="' . esc_textarea($player_ratings) . '" class="common_width">';
}

function wpt_regstatus()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'Register status');
    // Get the location data if it's already been entered
    $player_regstatus = get_post_meta($post->ID, 'player_regstatus', true);
    // Output the field
    echo '<select  name="player_regstatus" value="' . esc_textarea($player_regstatus) . '" class="common_width"
                                <option>Select a Reg status</option>                            
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>                          
                                <option value="Processing">Processing</option>                          
                            </select>';
}

function wpt_gender()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'gender');
    // Get the location data if it's already been entered
    $player_gender = get_post_meta($post->ID, 'player_gender', true);
    // Output the field
    echo '<select  name="player_gender" value="' . esc_textarea($player_gender) . '" class="common_width"
                                <option>Select a State</option>                            
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>                          
                                <option value="Other">Other</option>                          
                            </select>';
}

/**
 * Save the players metabox data
 */
function wpt_save_players_meta($post_id, $post)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    if ($parent_id = wp_is_post_revision($post_id)) {
        $post_id = $parent_id;
    }

    $players_meta['player_name'] = esc_textarea($_POST['player_name']);
    $players_meta['player_title'] = esc_textarea($_POST['player_title']);
    $players_meta['player_state'] = esc_textarea($_POST['player_state']);
    $players_meta['player_ratings'] = esc_textarea($_POST['player_ratings']);
    $players_meta['player_regstatus'] = esc_textarea($_POST['player_regstatus']);
    $players_meta['player_gender'] = esc_textarea($_POST['player_gender']);

    foreach ($players_meta as $key => $value) :
        if ('revision' === $post->post_type) {
            return;
        }
        if (get_post_meta($post_id, $key, false)) {
            update_post_meta($post_id, $key, $value);
        } else {
            add_post_meta($post_id, $key, $value);
        }
        if (!$value) {
            delete_post_meta($post_id, $key);
        }
    endforeach;
}

add_action('save_post', 'wpt_save_players_meta', 1);




