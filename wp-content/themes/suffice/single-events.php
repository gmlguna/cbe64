<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ThemeGrill
 * @subpackage Suffice
 * @since Suffice 1.0.0
 */
get_header(); ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 comment-section-here">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<?php
$ID = get_the_ID();
$ID = get_the_ID();
$args = array('p' => $ID, 'post_type' => 'about');
$loop = new WP_Query($args);
?>
<?php /* The loop */ ?>

<?php while (have_posts()) : the_post(); ?>
    <div class="main-post-div">
        <div class="single-page-post-heading">
            <h1><?php the_title(); ?></h1>
        </div>
<!--        <div class="content-here">-->
<!--            --><?php
//            the_content();
//            the_post_thumbnail('thumbnail');
//            ?>
<!--        </div>-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <strong>Start Date:</strong> <?php echo esc_attr(get_post_meta(get_the_ID(), 'event_start_date', true)); ?><br>
                <br>
                <strong>End Date: </strong><?php echo esc_attr(get_post_meta(get_the_ID(), 'event_end_date', true)); ?><br>
                <br>
                <strong>Time: </strong><?php echo esc_attr(get_post_meta(get_the_ID(), 'event_time', true)); ?><br>
                <br>
                <strong>Location: </strong><?php echo esc_attr(get_post_meta(get_the_ID(), 'event_location', true)); ?><br>
                <br>
                <strong>Event Code: </strong><?php echo esc_attr(get_post_meta(get_the_ID(), 'event_event_code', true)); ?><br>
                <br>
                <strong>Prize: </strong><?php echo esc_attr(get_post_meta(get_the_ID(), 'event_prize', true)); ?><br>
                <br>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <strong>Organizer
                    details: </strong><?php echo esc_attr(get_post_meta(get_the_ID(), 'event_organizer_details', true)); ?><br>
                <br>
                <strong>Email: </strong><?php echo esc_attr(get_post_meta(get_the_ID(), 'event_email', true)); ?>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/09/cher.png"
                     alt="player">
            </div>
        </div>


    </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  padd_top_20">
        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                     class="player_img" alt="player">
            </div>
            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
        </div>
        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                     class="player_img" alt="player">
            </div>
            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <a class="player_search" href="http://cis.fide.com/"> Chess in Schools</a>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
        </div>
        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                     class="player_img" alt="player">
            </div>
            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
        </div>
        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                     class="player_img" alt="player">
            </div>
            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
        </div>
        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                     class="player_img" alt="player">
            </div>
            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
        </div>
        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                     class="player_img" alt="player">
            </div>
            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card
                    holders</a>
            </div>
        </div>



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

            <?php $args = array(
                'posts_per_page' => 1,
                'offset' => 0,
                'category' => '',
                'category_name' => 'hurry',
                'orderby' => 'rand',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'author' => '',
                'author_name' => '',
                'post_status' => 'publish',
                'suppress_filters' => true,
                'fields' => '',
            );
            $posts_array = get_posts($args);
            if ($posts_array) {
                foreach ($posts_array as $post) :setup_postdata($post); ?>

                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_20 padd_left_right_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <img src="<?php echo $image[0]; ?>" alt="<?php echo the_title(); ?>">
                        </div>
                    </div>
                <?php
                endforeach;
                wp_reset_postdata();
            }
            ?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                   href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is
                    arranged towards confirmation of <br> entry : Phone numbers : +917358534422 /
                    +918610193178</a></div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                   href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                    PAYMENTS </a>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                   href="http://wordpress.lan/faq/">FAQ</a>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                News
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

            <?php $args = array(
                'posts_per_page' => 3,
                'offset' => 0,
                'category' => '',
                'category_name' => 'news',
                'orderby' => 'rand',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'author' => '',
                'author_name' => '',
                'post_status' => 'publish',
                'suppress_filters' => true,
                'fields' => '',
            );
            $posts_array = get_posts($args);
            if ($posts_array) {
                foreach ($posts_array as $post) :setup_postdata($post); ?>

                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10 latest-news-excerpt">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none ">
                                <img src="<?php echo $image[0]; ?>">
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12  padd_left_right_none">
                            </div>

                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  padd_left_right_none">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                   href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
                                <div><span class="latest-news-date"><i class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span></div>

                            </div>


                        </div>

                    </div>


                <?php
                endforeach;
                wp_reset_postdata();
            }
            ?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                Featured Events
            </div>
        </div>
        <?php
        // First lets set some arguments for the query:
        // Optionally, those could of course go directly into the query,
        // especially, if you have no others but post type.
        $args = array(
            'post_type' => 'events',
            'orderby' => 'rand',
            'posts_per_page' => 4
            // Several more arguments could go here. Last one without a comma.
        );
        // Query the posts:
        $featured_query = new WP_Query($args);

        // Loop through the obituaries:
        while ($featured_query->have_posts()) : $featured_query->the_post();   ?>
            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <img src="<?php echo $image[0]; ?>">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="<?php the_permalink(); ?>">
                        <?php the_title(); ?></a>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                        <div class="news-date-meta"><span class="latest-news-date"><i
                                        class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span></div>
                    </div>
                </div>
            </div>

        <?php endwhile; wp_reset_postdata(); ?>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="https://ratings.fide.com/title_applications.phtml">Title Applications</a>
            </div>
        </div>

        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                     class="player_img" alt="player">
            </div>
            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS</div>
            </div>
            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                     class="player_img" alt="player">
            </div>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                <div id="circle">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong>76612</strong>
                    </div>
                    <span>Registered Players</span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                <div id="circle">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong>28574</strong>
                    </div>
                    <span>FIDE Rated Players</span>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                <div id="circle">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong>219</strong>
                    </div>
                    <span>Tournaments in 2018</span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                <div id="circle">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong>51</strong>
                    </div>
                    <span> Grand Masters </span>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                <div id="circle">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                        <strong>101</strong>
                    </div>
                    <span> International Masters </span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                <div id="circle">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                        <strong>7</strong>
                    </div>
                    <span> Women Grand Masters </span>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                AICF Chronicles
            </div>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

            <?php $args = array(
                'posts_per_page' => 1,
                'offset' => 0,
                'category' => '',
                'category_name' => 'Chronicles',
                'orderby' => 'rand',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'author' => '',
                'author_name' => '',
                'post_status' => 'publish',
                'suppress_filters' => true,
                'fields' => '',
            );
            $posts_array = get_posts($args);
            if ($posts_array) {
            foreach ($posts_array as $post) :setup_postdata($post); ?>

            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>



            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">
                <?php echo get_the_date(); ?></a>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                    <a  href="<?php the_permalink(); ?>"> <img src="<?php echo $image[0]; ?>"> </a>
                </div>
                <?php
                endforeach;
                wp_reset_postdata();
                }
                ?>
            </div>

        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

            <?php $args = array(
                'posts_per_page' => 1,
                'offset' => 0,
                'category' => '',
                'category_name' => 'meta',
                'orderby' => 'rand',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'author' => '',
                'author_name' => '',
                'post_status' => 'publish',
                'suppress_filters' => true,
                'fields' => '',
            );
            $posts_array = get_posts($args);
            if ($posts_array) {
            foreach ($posts_array as $post) :setup_postdata($post); ?>

            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>


            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                        Mate in two moves
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <a  href="<?php the_permalink(); ?>"> <img src="<?php echo $image[0]; ?>"> </a>
                    </div>
                    <?php
                    endforeach;
                    wp_reset_postdata();
                    }
                    ?>
                </div>
        </div>
    </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>