<?php
/*
  Template Name: All Events
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_left_none announce">
                                All Events
                            </div>
                            <hr>
                        </div>

                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover">
                            <tbody>
                            <tr>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Location</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td><a class="player-table-values" <a href="">46th
                                        Tamilnadu State Women Chess Championship - 2018 - 176378 / TN / 2018</a></td>
                                <td class="date">02 Jan - 06 Jan</td>
                                <td class="location">Thirumathy Rasammal Thirumana Mandapam,Thepakulam South
                                    Bank,Tiruvarur,TN
                                </td>
                                <td class="url"><a href="" title="Email"><i class="fa fa-envelope" aria-hidden="true"></i> </a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href="" title="Delhi International Open 2018 – 154104 / 154105 / 154106 / DEL / 2018">Delhi
                                        International Open 2018 - 154104 / 154105 / 154106 / DEL / 2018</a></td>
                                <td class="date">09 Jan - 16 Jan</td>
                               <td class="location">Indira Gandhi Indoor Stadium, IP Estate</td>
                                <td class="url"><a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a> <a class="down" href=""><i class="fa fa-download" aria-hidden="true"></i></a></td>
                            </tr>


                            <tr>
                                <td><a class="player-table-values"  href=""

                                            title="9th State level Rating Chess tmt at Arunachalapradesh – 178097 / ARU / 2018">9th
                                        State level Rating Chess tmt at Arunachalapradesh - 178097 / ARU / 2018</a></td>
                                <td class="date">11 Jan - 15 Jan</td>
                                <td class="location">Siddhartha Hall Near Gompa Mandir, "O" point Itanagar</td>
                                <td class="url"><a href="mailto:tapishah81@gmail.com" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>


                            <tr>
                                <td><a class="player-table-values"
                                       href=""
                                            title="10th Chennai Open Grandmaster Chess Tournament – 165764 / TN / 2018">10th
                                        Chennai Open Grandmaster Chess Tournament - 165764 / TN / 2018</a></td>
                                <td class="date">18 Jan - 25 Jan</td>
                                <td class="location">Hotel The Vijay Park, 12, Jawaharlal Nehru Salai, Inner Ring Road,
                                    Arumbakkam, Chennai, 600106
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            title="Captain All West Bengal FIDE Rated Amateur below 2000 – 178165 / WB / 2018">Captain
                                        All West Bengal FIDE Rated Amateur below 2000 - 178165 / WB / 2018</a></td>
                                <td class="date">22 Jan - 25 Jan</td>
                                <td class="location">ICCR, Kolkata,9A Ho Chi minh sarani ,Kolkata,700071,033-22822895
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="Maharashtra State Open Team selection  – 180464 / MAH (s) / 2018">Maharashtra
                                        State Open Team selection - 180464 / MAH (s) / 2018</a></td>
                                <td class="date">25 Jan - 28 Jan</td>
                                <td class="location">Ram Mandir Trust,Near jogeshwari highway, Jogeshwari East
                                    ,Mumbai,MH
                                </td>
                                <td class="url"></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""

                                            title="3rd Karur FIDE Rating chess tmt below 1600 – 172201 / TN / 2017">3rd
                                        Karur FIDE Rating chess tmt below 1600 - 172201 / TN / 2017</a></td>
                                <td class="date">26 Jan - 28 Jan</td>
                                <td class="location">Atlas Kalai Arangam Near Koryu College, Karur - Salem Bye Pass
                                    Road, Karur (TN) - 639006
                                </td>
                                <td class="url"><a
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            title="Sri Gurukul FIDE Rating Chess Tmt  – 176788/AP/2018">Sri
                                        Gurukul FIDE Rating Chess Tmt - 176788/AP/2018</a></td>
                                <td class="date">26 Jan - 28 Jan</td>
                                <td class="location">Zilla Parishat Meeting Hall, Visakhapatnam, Andhra Pradesh</td>
                                <td class="url"><a href=""><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            title="Ganesan Memorial FIDE Rated Open – 170805 / TN / 2018">Ganesan
                                        Memorial FIDE Rated Open - 170805 / TN / 2018</a></td>
                                <td class="date">26 Jan - 29 Jan</td>
                                <td class="location">A.S.T.Kalyana Mandapam, Nagercoil</td>
                                <td class="url"><a href=""
                                                   title="Email"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    </a><a class="down" href=""
                                           title="Brochure" download=""><i class="fa fa-download"
                                                                           aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="Suraj FIDE Rating Open Chess tournament – 2018 – 177029/MAH/2018">Suraj
                                        FIDE Rating Open Chess tournament - 2018 - 177029/MAH/2018</a></td>
                                <td class="date">26 Jan - 30 Jan</td>
                                <td class="location">Krishna Valley Badmintan Hall,Near Krishna Valley Chember,
                                    Opp.Chakan Oil Mill, MIDC, Kupwad,Sangli,MH
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>
                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="Karippaparambil KC Sebastain memorial below 1500 FIDE – 170755 / KER / 2018">Karippaparambil
                                        KC Sebastain memorial below 1500 FIDE - 170755 / KER / 2018</a></td>
                                <td class="date">26 Jan - 28 Jan</td>
                                <td class="location">Benjamin Bailey hall, Kottayam</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            title="Shri Ram School FIDE Rating open – 174349 / TN / 2018">Shri Ram
                                        School FIDE Rating open - 174349 / TN / 2018</a></td>
                                <td class="date">26 Jan - 29 Jan</td>
                                <td class="location">Shri Ram School Pondy Main road, Omandur, Tindivanam, Villupuram
                                    (Dt )
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="26th The Telegraph schools’ Chess FIDE rated – 177532 / WB / 2018">26th
                                        The Telegraph schools' Chess FIDE rated - 177532 / WB / 2018</a></td>
                                <td class="date">27 Jan - 01 Feb</td>
                                <td class="location">Gorky Sadan, 3,Gorky Terrace, Kolkata,Gorky Sadan, 3,Gorky Terrace,
                                    Kolkata,West Bengal
                                </td>
                                <td class="url"><a href=""><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="2nd Nithm all India Open FIDE Rating Chess Tmt – 173680 / TEL / 2018">2nd
                                        Nithm all India Open FIDE Rating Chess Tmt - 173680 / TEL / 2018</a></td>
                                <td class="date">28 Jan - 01 Feb</td>
                                <td class="location">NITHM Campus (National Institute of Tourism Hospitality Management)
                                    New Urdhu University, Telecomnagar, Gachibowli, Hyderabad, Telangana
                                </td>
                                <td class="url"><a href=""
                                                   title="Email"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    </a><a class="down" href=""
                                           title="Brochure" download=""><i class="fa fa-download"
                                                                           aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"<a href=""
                                                           rel="bookmark"
                                                           title="National Schools 2018 – 175406/175407/175408/175409/175410/175411 /175413 / 175414 / 175416 / 175417 / 175418 / 175420 / ORI / 2018)">National
                                        Schools 2018 - 175406/175407/175408/175409/175410/175411 /175413 / 175414 /
                                        175416 / 175417 / 175418 / 175420 / ORI / 2018)</a></td>
                                <td class="date">28 Jan - 01 Feb</td>
                                <td class="location">KIIT University, Bhubaneshwar,Orissa</td>
                                <td class="url"><a
                                            href=""
                                            title="Email"><i class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"<a href=""
                                            rel="bookmark"
                                            title="18th North East Chess Championship – 178628 / NE / 2018">18th North
                                        East Chess Championship - 178628 / NE / 2018</a></td>
                                <td class="date">29 Jan - 03 Feb</td>
                                <td class="location">Laitumukrah, Shillong</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="1st Ankit Sakshi Memorial Open FIDE Rating – 2018 – 176262 / RAJ / 2017">1st
                                        Ankit Sakshi Memorial Open FIDE Rating - 2018 - 176262 / RAJ / 2017</a></td>
                                <td class="date">29 Jan - 02 Feb</td>
                                <td class="location">Maharishi Bhrigu Sadan Sunder Marg ,Tilak Nagar, Jaipur</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"<a href=""
                                            rel="bookmark"
                                            title="Dombivali Kalyan All India FIDE Rating Open – 178745 / MAH / 2018">Dombivali
                                        Kalyan All India FIDE Rating Open - 178745 / MAH / 2018</a></td>
                                <td class="date">30 Jan - 04 Feb</td>
                                <td class="location">Kalyan Sports Club, Wadhwa Complex, Near Durgadi Bridge, Aadharwadi
                                    Road, Kalyan West 421301
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="4th Teekay FIDE Chess Tournament – 174338 / TN / 2018">4th Teekay
                                        FIDE Chess Tournament - 174338 / TN / 2018</a></td>
                                <td class="date">31 Jan - 03 Feb</td>
                                <td class="location">Port Trust Community Hall VOC Port Trust, Thoothukudi</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="3rd Nithm All India below 1400 FIDE Rating Chess Tmt – 173681 / TEL / 2018">3rd
                                        Nithm All India below 1400 FIDE Rating Chess Tmt - 173681 / TEL / 2018</a></td>
                                <td class="date">02 Feb - 04 Feb</td>
                                <td class="location">NITHM Campus (National Institute of Tourism Hospitality Management)
                                    New Urdhu University, Telecomnagar, Gachibowli, Hyderabad, Telangana
                                </td>
                                <td class="url"><a href=""
                                                   title="Email"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    </a><a class="down" href=""
                                           title="Brochure" download=""><i class="fa fa-download"
                                                                           aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="AICFB National “A” Chess Championship for Visually Challenged 2018 – 178593 / AICFB / 2018">AICFB
                                        National "A" Chess Championship for Visually Challenged 2018 - 178593 / AICFB /
                                        2018</a></td>
                                <td class="date">03 Feb - 11 Feb</td>
                                <td class="location">Shahaji Raje Kreeda Sankul, Mira Desai Marg, Andheri West, Mumbai
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                                           rel="bookmark"
                                                           title="National Team 2018 – 177032/177033/ODI/2018">National
                                        Team 2018 - 177032/177033/ODI/2018</a></td>
                                <td class="date">07 Feb - 14 Feb</td>
                                <td class="location">Bhubaneshwar,Orissa</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="6th Sou.Nirmala Vaze memorial All India Open Rapid FIDE Rating  – 181415 / MAH (R)/2018">6th
                                        Sou.Nirmala Vaze memorial All India Open Rapid FIDE Rating - 181415 / MAH
                                        (R)/2018</a></td>
                                <td class="date">10 Feb - 11 Feb</td>
                                <td class="location">Wamanrao Muranjan High School hall,Sharda Nilayam, Neelaym Nagar,
                                    Phase 2, Behind Fire Brigade Station,Gawanpada, Mulund(East), Mumbai,MH,400081
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href="http://aicf.in/event/10th-brdca-fide-rating-rapid-178574-kar-2018/"
                                            rel="bookmark" title="10th BRDCA FIDE Rating Rapid – 178574 / KAR / 2018">10th
                                        BRDCA FIDE Rating Rapid - 178574 / KAR / 2018</a></td>
                                <td class="date">10 Feb - 11 Feb</td>
                                <td class="location">Virginia Mall, Kodi Circle, Whitefield main road, Bangalore
                                    -560066
                                </td>
                                <td class="url"><a
                                            href="http://aicf.in/wp-content/uploads/2017/12/10th-brdca-5-files-merged-1.pdf"
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="Open FIDE rating Chess Tournament for Visually Challenged – 178594 / AICFB / 2018">Open
                                        FIDE rating Chess Tournament for Visually Challenged - 178594 / AICFB / 2018</a>
                                </td>
                                <td class="date">13 Feb - 16 Feb</td>
                                <td class="location">Shahaji Raje Kreeda Sankul, Mira Desai Marg, Andheri West, Mumbai
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="1st Sri Anand Chess Wings All India Open FIDE Rating 181773 / AP / 2018">1st
                                        Sri Anand Chess Wings All India Open FIDE Rating 181773 / AP / 2018</a></td>
                                <td class="date">16 Feb - 19 Feb</td>
                                <td class="location">VVIT Engineering College, Pedda kakani, Nambaru, Guntur - 522508
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="Shri Dakshinamurthy  FIDE Rating below 1500 – 174350 / TN / 2018">Shri
                                        Dakshinamurthy FIDE Rating below 1500 - 174350 / TN / 2018</a></td>
                                <td class="date">23 Feb - 25 Feb</td>
                                <td class="location">Sri Dharam Chand Jain school Vandavasi main road- karuvambakkam
                                    Tindivanam Villupuram Dt
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="4th Kanyakumari FIDE Rated below 1500 – 170806 / TN / 2018">4th
                                        Kanyakumari FIDE Rated below 1500 - 170806 / TN / 2018</a></td>
                                <td class="date">23 Feb - 25 Feb</td>
                                <td class="location">Stella Mary's Engineering college Nagercoil</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>
                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="BPS All Goa Open Rapid FIDE Rating  – 181750 / GOA(R)/2018">BPS All
                                        Goa Open Rapid FIDE Rating - 181750 / GOA(R)/2018</a></td>
                                <td class="date">24 Feb - 25 Feb</td>
                                <td class="location">BPS Sports Club, Bernardo Pereda Silva Road, Goa</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="2nd Shaastra Rapid FIDE Rating – 170804 / TN(R) / 2018">2nd Shaastra
                                        Rapid FIDE Rating - 170804 / TN(R) / 2018</a></td>
                                <td class="date">24 Feb - 25 Feb</td>
                                <td class="location">Students Activity Centre, IIT Madras,Chennai</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="All Maharashtra State Rapid Rating – 184590 / MAH(S)(R)/2018">All
                                        Maharashtra State Rapid Rating - 184590 / MAH(S)(R)/2018</a></td>
                                <td class="date">17 Mar - 18 Mar</td>
                                <td class="location">Ram Mandir hall,Janata Colony, Jogeshwari East,Mumbai,MH</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="All Maharashtra State Blitz Rating – 184591 / MAH(S)(B)/2018">All
                                        Maharashtra State Blitz Rating - 184591 / MAH(S)(B)/2018</a></td>
                                <td class="date">18 Mar - 18 Mar</td>
                                <td class="location">Ram Mandir hall,Janata Colony, Jogeshwari East,Mumbai,MH</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                       href=""
                                            rel="bookmark"
                                            title="2nd Dalmia Cements All India Open FIDE Rating  – 181783 / JHAR / 2018">2nd
                                        Dalmia Cements All India Open FIDE Rating - 181783 / JHAR / 2018</a></td>
                                <td class="date">26 Mar - 31 Mar</td>
                                <td class="location">Birla Public School BIRLA CAMPUS, Ranchi - Purulia Road, Village -
                                    ARA, P. O. - Mahilong, Ranchi - 835103
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark" title="KCA 16th Open FIDE Rating – 179661 / KER / 2018">KCA
                                        16th Open FIDE Rating - 179661 / KER / 2018</a></td>
                                <td class="date">29 Mar - 01 Apr</td>
                                <td class="location">Mammen Mappila Hall,Kottayam,686001</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="2nd Holi Cup Lakecity open FIDE Rating – 176823 / RAJ / 2018">2nd
                                        Holi Cup Lakecity open FIDE Rating - 176823 / RAJ / 2018</a></td>
                                <td class="date">29 Mar - 01 Apr</td>
                                <td class="location">BHANDARI DARSHAK MANDAP,GANDHI GROUND ,Udaipur,RJ</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="West Bengal State Under 11 Open &amp; Girls FIDE rated – 183470 / 183471 / WB(S) /2018">West
                                        Bengal State Under 11 Open &amp; Girls FIDE rated - 183470 / 183471 / WB(S)
                                        /2018</a></td>
                                <td class="date">29 Mar - 31 Mar</td>
                                <td class="location">Indian council for Cultural Relations,Kolkata,WB</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"
                                            href=""
                                            rel="bookmark"
                                            title="Asian Youth Chess Championships – 2018 U 8,10,12,14,16 and 18">Asian
                                        Youth Chess Championships - 2018 U 8,10,12,14,16 and 18</a></td>
                                <td class="date">31 Mar - 10 Apr</td>
                                <td class="location">Chiangmai Thailand</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"  href=""

                                            rel="bookmark" title="1st TCS FIDE Rated open – 181045 / HAR / 2018">1st TCS
                                        FIDE Rated open - 181045 / HAR / 2018</a></td>
                                <td class="date">02 Apr - 07 Apr</td>
                                <td class="location">Chaudhary Charan Singh Haryana Agriculture University, Hisar -
                                    125001
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""

                                            rel="bookmark"
                                            title="44th West Bengal State women Chess Championship 2018 – 183174 / WB(S) / 2018">44th
                                        West Bengal State women Chess Championship 2018 - 183174 / WB(S) / 2018</a></td>
                                <td class="date">06 Apr - 08 Apr</td>
                                <td class="location">Sanjojani club, Kumartuli, Kolkata - 700075</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values"  href=""

                                            rel="bookmark"
                                            title="1st Avinash Group FIDE Rated Open State chess Championship – 183081 / CHHAT / 2018">1st
                                        Avinash Group FIDE Rated Open State chess Championship - 183081 / CHHAT /
                                        2018</></td>
                                <td class="date">06 Apr - 08 Apr</td>
                                <td class="location">Club Paraiso Maruti,lifestyle kota road,Raipur,CT</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""

                                            rel="bookmark" title="3rd Don Bosco FIDE Rated – 179757 / KER / 2018">3rd
                                        Don Bosco FIDE Rated - 179757 / KER / 2018</a></td>
                                <td class="date">07 Apr - 10 Apr</td>
                                <td class="location">Don bosco School,Irinjalakuda,KL</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""

                                            rel="bookmark"
                                            title="Rajasthan State Senior FIDE Rated  – 184855 / RAJ (S) / 2018">Rajasthan
                                        State Senior FIDE Rated - 184855 / RAJ (S) / 2018</a></td>
                                <td class="date">07 Apr - 10 Apr</td>
                                <td class="location">Frame International School, Nagal Bairsi Road, Dausa</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st Brain Master Open FIDE Rated – 182456 / MP / 2018">1st Brain
                                        Master Open FIDE Rated - 182456 / MP / 2018</a></td>
                                <td class="date">08 Apr - 12 Apr</td>
                                <td class="location">SG STADIUM near Bus stand Datiya</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""

                                            rel="bookmark"
                                            title="Delhi State FIDE Rated Chess Tournament 2018 – 185150 / DEL (S) / 2018">Delhi
                                        State FIDE Rated Chess Tournament 2018 - 185150 / DEL (S) / 2018</a></td>
                                <td class="date">09 Apr - 15 Apr</td>
                                <td class="location">Jawahar Lal Nehru Stadium, Delhi,</td>
                                <td class="url"><a href=".com" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="7th Keshabananda Das Memorial All India Open – 184594 / ORI / 2018">7th
                                        Keshabananda Das Memorial All India Open - 184594 / ORI / 2018</a></td>
                                <td class="date">10 Apr - 14 Apr</td>
                                <td class="location">Hotel Empires,Saheed Nagar,Bhubaneswar,OR,751007</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="2nd Sri Anand Chess Wings All India Open FIDE Rating below 1500 – 181778 / AP / 2018">2nd
                                        Sri Anand Chess Wings All India Open FIDE Rating below 1500 - 181778 / AP /
                                        2018</a></td>
                                <td class="date">13 Apr - 15 Apr</td>
                                <td class="location">Krishnudi Kalyana Mandapam, Old Guntur, Back side Brahmananda
                                    Stadium, Guntur - 522001
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark" title="1st IITM Open FIDE Rating – 185210 / TN / 2018">1st
                                        IITM Open FIDE Rating - 185210 / TN / 2018</a></td>
                                <td class="date">13 Apr - 15 Apr</td>
                                <td class="location">IIT Madras,Sardar Patel Road,Chennai,TN,600020</td>
                                <td class="url"><a class="down" href=""
                                                   title="Brochure" download=""><i class="fa fa-download"
                                                                                   aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Karnataka State Rated Open Chess Championship – 180144 / KAR(S) / 2018">Karnataka
                                        State Rated Open Chess Championship - 180144 / KAR(S) / 2018</a></td>
                                <td class="date">14 Apr - 18 Apr</td>
                                <td class="location">Koramangala Indoor Stadium</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="West Bengal State FIDE Rated Rapid – 184028 / WB(S) R/ 2018">West
                                        Bengal State FIDE Rated Rapid - 184028 / WB(S) R/ 2018</a></td>
                                <td class="date">14 Apr - 16 Apr</td>
                                <td class="location">College of Engineering &amp; management, Kolaghat. Kolghat, Purba
                                    Medinipur, Pin 721170
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="2nd Sardar Prakash Singh Memorial FIDE Rated – 180463 / HAR / 2018">2nd
                                        Sardar Prakash Singh Memorial FIDE Rated - 180463 / HAR / 2018</a></td>
                                <td class="date">17 Apr - 22 Apr</td>
                                <td class="location">swarnprastha public school National Highway-1 sonepat</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="CG State Under – 13 Open Selection Chess tmt – 186076 / CHAT (S) / 2018">CG
                                        State Under - 13 Open Selection Chess tmt - 186076 / CHAT (S) / 2018</a></td>
                                <td class="date">20 Apr - 22 Apr</td>
                                <td class="location">Alliance institute near agrasen Bhawan sector 6 bhilai</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Karnataka State Under 15 &amp; U 15 Girls FIDE Rated – 183278 / 183275 / KAR(S)/2018">Karnataka
                                        State Under 15 &amp; U 15 Girls FIDE Rated - 183278 / 183275 / KAR(S)/2018</a>
                                </td>
                                <td class="date">20 Apr - 22 Apr</td>
                                <td class="location">Hotel Paradise Yadavagiri, Mysore-570020</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Mariappan Nadar Memorial FIDE Rated – 182236 / TN / 2018">Mariappan
                                        Nadar Memorial FIDE Rated - 182236 / TN / 2018</a></td>
                                <td class="date">21 Apr - 24 Apr</td>
                                <td class="location">Jawaharlal Nehru Stadium,Periamet, Chennai,600003</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Hatsun – Idhayam 4th SCS FIDE Rated – 184596 / TN / 2018">Hatsun -
                                        Idhayam 4th SCS FIDE Rated - 184596 / TN / 2018</a></td>
                                <td class="date">21 Apr - 24 Apr</td>
                                <td class="location">Mepco Schlenk Engineering College, Mepco Nagar, Sivakasi</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st ARMS Open Rapid FIDE Rating – 178902 / MAH (R) / 2018">1st ARMS
                                        Open Rapid FIDE Rating - 178902 / MAH (R) / 2018</a></td>
                                <td class="date">21 Apr - 22 Apr</td>
                                <td class="location">Ballroom Pallazzo AC Hall 1st Floor Metro Junction Mall Netivali
                                    Road, Kalyan East Thane 421 301
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Late Sou. Meenatai Shirgaokar FIDE Rating Open women’s – 184023 / MAH / 2018">Late
                                        Sou. Meenatai Shirgaokar FIDE Rating Open women's - 184023 / MAH / 2018</a></td>
                                <td class="date">24 Apr - 28 Apr</td>
                                <td class="location">Dr.Bapat Bal Shikshan Mandir,Shivaji Mandal,Sangli,MH</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="5th Kuruksheta Open FIDE rated – 184595 / ASSM / 2018">5th Kuruksheta
                                        Open FIDE rated - 184595 / ASSM / 2018</a></td>
                                <td class="date">25 Apr - 30 Apr</td>
                                <td class="location">DHSK College, Debrugarh, Khalihamari, Assam</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="G H Raisoni Memorial Fide Rating Chess Tournament 2018 – 186510 / MAH / 2018">G
                                        H Raisoni Memorial Fide Rating Chess Tournament 2018 - 186510 / MAH / 2018</a>
                                </td>
                                <td class="date">26 Apr - 01 May</td>
                                <td class="location">Dr, B.R. Ambedkar College of Management, (New Building)
                                    Deekshabhoomi, Laxminagar Square, Nagpur:440010
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="TN State Open Chess Championship 2018 – 178571 / TN / 2018">TN State
                                        Open Chess Championship 2018 - 178571 / TN / 2018</a></td>
                                <td class="date">26 Apr - 30 Apr</td>
                                <td class="location">Kamalam International School K.Naduhalli Palacode Road, (Near NH
                                    fly Over) dharmapuri
                                </td>
                                <td class="url"><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>

                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="CHESSMATE ( Inter School ) FIDE Rating Rapid  – 185879 / WB (R) / 2018">CHESSMATE
                                        ( Inter School ) FIDE Rating Rapid - 185879 / WB (R) / 2018</a></td>
                                <td class="date">27 Apr - 28 Apr</td>
                                <td class="location">South Point high School, 82/7A, Ballygunge Place, Kolkataa -
                                    700019.
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="4 Queens 1st open FIDE Chess Tournament – 174083 / KER / 2018">4
                                        Queens 1st open FIDE Chess Tournament - 174083 / KER / 2018</a></td>
                                <td class="date">28 Apr - 01 May</td>
                                <td class="location">BEITH CONVENTION CENTRE, CUSAT JN, ERNAKULAM, KERALA</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st MyBrain All India Open Rapid FIDE Rating – 185865 / MAH(R)/2018">1st
                                        MyBrain All India Open Rapid FIDE Rating – 185865 / MAH(R)/2018</a></td>
                                <td class="date">28 Apr - 29 Apr</td>
                                <td class="location">Universal High School, Chikalthana MIDC, Behind Wokhardt,
                                    Aurangabad
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Late Babukaka Shirgaokar FIDE Rating Open – 184024 / MAH / 2018">Late
                                        Babukaka Shirgaokar FIDE Rating Open - 184024 / MAH / 2018</a></td>
                                <td class="date">29 Apr - 04 May</td>
                                <td class="location">Dr.Bapat Bal Shikshan Mandir,Shivaji Mandal,Sangli,MH</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st IGMSA All India Open FIDE Rating Chess Tournament  – 171462 / TEL / 2018">1st
                                        IGMSA All India Open FIDE Rating Chess Tournament - 171462 / TEL / 2018</a></td>
                                <td class="date">29 Apr - 04 May</td>
                                <td class="location">Safilguda Circle, Santoshima Nagar, R.K.Puram, Secunderabad,
                                    Telangana
                                </td>
                                <td class="url"><a href=""
                                                   title="Email"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    </a><a class="down" href="" title="Brochure"
                                           download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="TN State Under 13 Open &amp; Girls – 182234 / 182235 / TN(S)/2018">TN
                                        State Under 13 Open &amp; Girls - 182234 / 182235 / TN(S)/2018</a></td>
                                <td class="date">01 May - 05 May</td>
                                <td class="location">Dr N G P College of Arts &amp; Science, Kalapatti Road,
                                    Coimbatore
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                                           rel="bookmark"
                                                           title="National Rapid Chess Championship 2018   – 182238 / GUJ / 2018">National
                                        Rapid Chess Championship 2018 - 182238 / GUJ / 2018</a></td>
                                <td class="date">02 May - 04 May</td>
                                <td class="location">Gujarat State Co.op. Bank Ltd. (A.C. Hall), Sahakar Bhavan, 132 ft.
                                    Ring Road, Nr. Pallav Cross Road, Naranpura, Ahmedabad 380013
                                </td>
                                <td class="url"><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="National FIDE Rating Open Chess tournament for the visually challenged – 186823 / AICFB / 2018">National
                                        FIDE Rating Open Chess tournament for the visually challenged - 186823 / AICFB /
                                        2018</a></td>
                                <td class="date">02 May - 06 May</td>
                                <td class="location">Devnar School for the Blind,Begumpet,Hyderabad</td>
                                <td class="url"><a href="" title="Email"><i class="fa fa-envelope"
                                                                                                  aria-hidden="true"></i>
                                    </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Bengal State Under 13 open &amp; Girls FIDE rating – 185957 / 185958 / WB(S) / 2018">Bengal
                                        State Under 13 open &amp; Girls FIDE rating - 185957 / 185958 / WB(S) / 2018</a>
                                </td>
                                <td class="date">04 May - 06 May</td>
                                <td class="location">Sahid Smriti Sangha, Dhakuria, Kolkata - 31</td>
                                <td class="url"><a
                                            href=""
                                            title="Email"><i class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="KCA 17th below 1400 FIDE Rating – 179662 / KER / 2018">KCA 17th below
                                        1400 FIDE Rating - 179662 / KER / 2018</a></td>
                                <td class="date">04 May - 06 May</td>
                                <td class="location">Benjamin Bailey hall, Kottayam</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Late Bharatbai Halkude Memorial 1st All India FIDE Rating – 178569 / MAH / 2018">Late
                                        Bharatbai Halkude Memorial 1st All India FIDE Rating - 178569 / MAH / 2018</a>
                                </td>
                                <td class="date">05 May - 10 May</td>
                                <td class="location">Ganesh Kala Krida Manch Near Neharu Stadium, Swargate, Pune</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="National blitz chess championship 2018   – 182239 /GUJ / 2018">National
                                        blitz chess championship 2018 - 182239 /GUJ / 2018</a></td>
                                <td class="date">05 May - 05 May</td>
                                <td class="location">Gujarat State Co.op. Bank Ltd. (A.C. Hall), Sahakar Bhavan, 132 ft.
                                    Ring Road, Nr. Pallav Cross Road, Naranpura, Ahmedabad 380013
                                </td>
                                <td class="url"><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="2nd IGMSA All India Chess Tournament Below &nbsp;1500 – 171463 / TEL / 2018">2nd
                                        IGMSA All India Chess Tournament Below &nbsp;1500 - 171463 / TEL / 2018</a></td>
                                <td class="date">05 May - 07 May</td>
                                <td class="location">SRINIVASA FUNCTION HALL NAGOLE ROAD, KOTHAPET, NEAR MAHALAKSHMI
                                    THEATER HYDERABAD-500035 PHONE:040-2405530
                                </td>
                                <td class="url"><a href=""
                                                   title="Email"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    </a><a class="down" href=""
                                           title="Brochure" download=""><i class="fa fa-download"
                                                                           aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            href=""
                                            rel="bookmark"
                                            title="TN State Sub Junior Open &amp; Girls Chess Championship – 187600 / 187601 /TN(S) / 2018">TN
                                        State Sub Junior Open &amp; Girls Chess Championship - 187600 / 187601 /TN(S) /
                                        2018</a></td>
                                <td class="date">06 May - 10 May</td>
                                <td class="location">Koviloor Andavar Matrick School, kovilar, Karaikudi</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Chess in Lakecity Summer Cup FIDE Rating below 1700 – 176824 / RAJ / 2018">Chess
                                        in Lakecity Summer Cup FIDE Rating below 1700 - 176824 / RAJ / 2018</a></td>
                                <td class="date">08 May - 10 May</td>
                                <td class="location">BHANDARI DARSHAK MANDAP,GANDHI GROUND ,Udaipur,RJ</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Akhil Smriti 44th Tripura State FIDE Rating  – 187897/ TRI(S) / 2018">Akhil
                                        Smriti 44th Tripura State FIDE Rating - 187897/ TRI(S) / 2018</a></td>
                                <td class="date">09 May - 13 May</td>
                                <td class="location">"SATHI" Godachowmuhani, Chanban Udaipur, Gomati District</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="All India Open FIDE Rating Tournament – 182674 / TEL / 2018">All
                                        India Open FIDE Rating Tournament - 182674 / TEL / 2018</a></td>
                                <td class="date">10 May - 14 May</td>
                                <td class="location">Pallavi international School(VIF Engineering College)Gandipet,
                                    Hyderabad
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="49th TN State Junior and 32nd TN State Junior girls – 187605 / 187606 / TN(S) / 2018">49th
                                        TN State Junior and 32nd TN State Junior girls - 187605 / 187606 / TN(S) /
                                        2018</a></td>
                                <td class="date">11 May - 15 May</td>
                                <td class="location">PSNA Engeneering College and Technology, Kothandaraman Nagar,Palani
                                    Road,Dindigul-624622
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Late Bharatbai Halkude Memorial 2nd All India FIDE Rating – below 1600 – 178570 / MAH / 2018">Late
                                        Bharatbai Halkude Memorial 2nd All India FIDE Rating - below 1600 - 178570 / MAH
                                        / 2018</a></td>
                                <td class="date">11 May - 13 May</td>
                                <td class="location">Ganesh Kala Krida Manch Near Neharu Stadium, Swargate, Pune</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="TN State Under – 11 Open &amp; Girls  – 2018  – 179204 / 179205 / TN / 2018">TN
                                        State Under - 11 Open &amp; Girls - 2018 - 179204 / 179205 / TN / 2018</a></td>
                                <td class="date">11 May - 15 May</td>
                                <td class="location">Kanchipuram</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark" title="Kasparov below 1600 FIDE Rated – 179758 / KER / 2018">Kasparov
                                        below 1600 FIDE Rated - 179758 / KER / 2018</a></td>
                                <td class="date">12 May - 14 May</td>
                                <td class="location">paremakavu Devaswam Vidya Mandir School, MLA Road, Thrissur</td>
                                <td class="url"><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Kolkata International Grandmaster Open tournament – 175441 / WB / 2018">Kolkata
                                        International Grandmaster Open tournament - 175441 / WB / 2018</a></td>
                                <td class="date">14 May - 22 May</td>
                                <td class="location">The New Town School, Premises 01-0279, Plot No. DD 257, Action Area
                                    1, New Town, Kolkata, West Bengal 700156
                                </td>
                                <td class="url"><a href=""
                                                   title="Email"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    </a><a class="down" href=""
                                           title="Brochure" download=""><i class="fa fa-download"
                                                                           aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Maharashtra State Senior open FIDE Rating  – 188192 /MAH(S) / 2018">Maharashtra
                                        State Senior open FIDE Rating - 188192 /MAH(S) / 2018</a></td>
                                <td class="date">14 May - 18 May</td>
                                <td class="location">Rangdarshan hall, Tilak Road, Pune</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="TN State Under – 17 Open &amp; Girls  – 2018  – 179206 / 179207 / TN / 2018">TN
                                        State Under - 17 Open &amp; Girls - 2018 - 179206 / 179207 / TN / 2018</a></td>
                                <td class="date">16 May - 20 May</td>
                                <td class="location">University College of Engineering, Konam, Nagercoil</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="31st TN State Under 09 Boys &amp; Girls Chess Championship 2018 – 185223/185225/TN(S)/2018">31st
                                        TN State Under 09 Boys &amp; Girls Chess Championship 2018 -
                                        185223/185225/TN(S)/2018</a></td>
                                <td class="date">16 May - 20 May</td>
                                <td class="location">SHIVANI ENGINEERING COLLEGE,DINDIGUL
                                    ROAD,POOLANGULATHUPATTI,TIRUCHIRAPPALLI -620009
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Karnataka State U-13 Open Chess Championship 2018 – 186825 / KAR(S)/2018">Karnataka
                                        State U-13 Open Chess Championship 2018 - 186825 / KAR(S)/2018</a></td>
                                <td class="date">17 May - 19 May</td>
                                <td class="location">Vokkaligara Bhavana, Opposite to Shimoga Dist Court, Court Circlem
                                    Shimoga, Karnataka
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Nagpur District Open FIDE Rating – 188568 / MAH (S) / 2018">Nagpur
                                        District Open FIDE Rating - 188568 / MAH (S) / 2018</a></td>
                                <td class="date">17 May - 22 May</td>
                                <td class="location">Naivedhyam Eastoria, Surya Nagar, Nagpur,Chikhli Kalamna Road,
                                    Surya Nagar,Nagpur
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Dr. C K Singh Memorial18th Jharkhand State SubJunior FIDE Rating Chess Championship – 188189 / JHAR(S)/2018">Dr.
                                        C K Singh Memorial18th Jharkhand State SubJunior FIDE Rating Chess Championship
                                        - 188189 / JHAR(S)/2018</a></td>
                                <td class="date">24 May - 27 May</td>
                                <td class="location">Maharaja Shree Agrasen Bhawan, Hirapur, Tell para, Dhanbad -
                                    826001
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="34th Haryana State Selection open ( men &amp; women ) championship – 2018  – 187663 / HAR(S)/2018">34th
                                        Haryana State Selection open ( men &amp; women ) championship - 2018 - 187663 /
                                        HAR(S)/2018</a></td>
                                <td class="date">24 May - 27 May</td>
                                <td class="location">Shriram Global School, B-block, Greenwood city, Sector-45,
                                    Gurgaon
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Odisha GM Open Chess Festival 2018 – 184430 / 184431 / 184432 / ORI / 2018">Odisha
                                        GM Open Chess Festival 2018 - 184430 / 184431 / 184432 / ORI / 2018</a></td>
                                <td class="date">25 May - 01 Jun</td>
                                <td class="location">KIIT University, Bhubaneshwar,Orissa</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr> <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st Pragatisheel Chhattisagarh Satnami Samaj State leve open – 186075 / Chat(S)/2018">1st
                                        Pragatisheel Chhattisagarh Satnami Samaj State leve open - 186075 /
                                        Chat(S)/2018</a></td>
                                <td class="date">27 May - 30 May</td>
                                <td class="location">Guru Ghasidas Sanskritik Bhavan, New Rajendra nager, Guru Ghasidas
                                    Society, Raipur (C.G)
                                </td>
                                <td class="url"><a href=""
                                                   title="Email"><i class="fa fa-envelope" aria-hidden="true"></i> </a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Dr. C K Singh Memorial 18th Jharkhand State Junior FIDE Rating Chess Championship – 188190 / JHAR(S)/2018">Dr.
                                        C K Singh Memorial 18th Jharkhand State Junior FIDE Rating Chess Championship -
                                        188190 / JHAR(S)/2018</a></td>
                                <td class="date">27 May - 30 May</td>
                                <td class="location">Maharaja Shree Agrasen Bhawan, Hirapur, Tell para, Dhanbad -
                                    826001
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="56th Kerala State Senior open – 188944 / KER(S) / 2018">56th Kerala
                                        State Senior open - 188944 / KER(S) / 2018</a></td>
                                <td class="date">30 May - 03 Jun</td>
                                <td class="location">Hotel Mermaid Inn, Vytila, Ernakulam</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Bengal State Under – 9 Boys &amp; Girls Chess Championship  – 187969 / 187971 / WB / 2018">Bengal
                                        State Under - 9 Boys &amp; Girls Chess Championship - 187969 / 187971 / WB /
                                        2018</a></td>
                                <td class="date">01 Jun - 03 Jun</td>
                                <td class="location">Khudiram Anusilan Kendra (A/C), Kolkata – 700021,Khudiram Anusilan
                                    Kendra (A/C), Kolkata - 700021,West Bengal
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="11th  Mayor’s Cup International GM event &amp; FIDE Rating open – 184048 / 184049 / 184050 / MAH / 2018">11th
                                        Mayor's Cup International GM event &amp; FIDE Rating open - 184048 / 184049 /
                                        184050 / MAH / 2018</a></td>
                                <td class="date">03 Jun - 10 Jun</td>
                                <td class="location">Mount Litera School,Near UTI Bidg. Bandra-Kurla Complex.,Bandra
                                    East, Mumbai,400051
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="The Heritage West Bengal State sub Junior Open &amp; Girls 2018 – 188803 / 188804 / WB(S) / 2018">The
                                        Heritage West Bengal State sub Junior Open &amp; Girls 2018 - 188803 / 188804 /
                                        WB(S) / 2018</a></td>
                                <td class="date">06 Jun - 09 Jun</td>
                                <td class="location">N/A</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="AICFB West Zone Selection Chess Tournament for the Blind  – 188740 / AICFB / 2018">AICFB
                                        West Zone Selection Chess Tournament for the Blind - 188740 / AICFB / 2018</a>
                                </td>
                                <td class="date">09 Jun - 12 Jun</td>
                                <td class="location">Jagruti Blind Girls School, devachi Alandi, Pune, Maharashtra</td>
                                <td class="url"><a href="mailto:office@aicfb.in" title="Email"><i class="fa fa-envelope"
                                                                                                  aria-hidden="true"></i>
                                    </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Green Acres FIDE Rating Chess Tournament – 191291 / MAH / 2018">Green
                                        Acres FIDE Rating Chess Tournament - 191291 / MAH / 2018</a></td>
                                <td class="date">12 Jun - 19 Jun</td>
                                <td class="location">The Acres Club, 411-B, Hemu Kalani Marg,Chembur (E), Mumbai – 400
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark" title="AICF WOMEN ROUND ROBIN – 181250 / AICF / MAH / 2018">AICF
                                        WOMEN ROUND ROBIN - 181250 / AICF / MAH / 2018</a></td>
                                <td class="date">12 Jun - 19 Jun</td>
                                <td class="location">Russian Center for Science &amp; Culture, Peddar Road, Mumbai -
                                    400026.
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="32nd National Under – 13 ( Open &amp; Girls ) 2018 – 182811 / 182812 / GUJ / 2018">32nd
                                        National Under - 13 ( Open &amp; Girls ) 2018 - 182811 / 182812 / GUJ / 2018</a>
                                </td>
                                <td class="date">14 Jun - 22 Jun</td>
                                <td class="location">Rajpath Club (A.C. Hall), S. G. Highway, Ahmedabad, Gujarat</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Maharashtra State under – 11 Open &amp; Girls – 188193 /188194 / MAH (S) / 2018">Maharashtra
                                        State under - 11 Open &amp; Girls - 188193 /188194 / MAH (S) / 2018</a></td>
                                <td class="date">14 Jun - 17 Jun</td>
                                <td class="location">Indira Gandhi Sankul, Korit Naka Road, Nandurbar - 425412</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st West Bengal State U 25 Open FIDE Rating – 190004 / WB(S) / 2018">1st
                                        West Bengal State U 25 Open FIDE Rating - 190004 / WB(S) / 2018</a></td>
                                <td class="date">14 Jun - 17 Jun</td>
                                <td class="location">Jadavpur University, AC Canteen Hall, Kolkatta</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Gandhi – Kamaraj Memorial FIDE Rating below 1600 – 182237 / TN / 2018">Gandhi
                                        - Kamaraj Memorial FIDE Rating below 1600 - 182237 / TN / 2018</a></td>
                                <td class="date">15 Jun - 17 Jun</td>
                                <td class="location">Jawaharlal Nehru Stadium,Periamet, Chennai,600003</td>
                                <td class="url"><a class="down" href=""
                                                   title="Brochure" download=""><i class="fa fa-download"
                                                                                   aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Tamilnadu State U -25 Open &amp; Girls – 187602 / 187603 / TN(S) / 2018">Tamilnadu
                                        State U -25 Open &amp; Girls - 187602 / 187603 / TN(S) / 2018</a></td>
                                <td class="date">15 Jun - 19 Jun</td>
                                <td class="location">Mahendra Salem Campus, Chennai Main Road, Minnampalli, Salem, Tamil
                                    Nadu 636106
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="All India below 1600 FIDE Rating Chess Tournament 2018 – 186512 / MAH / 2018">All
                                        India below 1600 FIDE Rating Chess Tournament 2018 - 186512 / MAH / 2018</a>
                                </td>
                                <td class="date">15 Jun - 17 Jun</td>
                                <td class="location">Dr. Hedgewar Smarak Samiti (Gate no. 2) ,Reshimbagh , NAGPUR</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>


                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st Bhagwan Mahaveer Open FIDE Rating – 188930 / MAH / 2018">1st
                                        Bhagwan Mahaveer Open FIDE Rating - 188930 / MAH / 2018</a></td>
                                <td class="date">19 Jun - 23 Jun</td>
                                <td class="location">Jaylaxmi Sanskrutik Bhavan, Near Khanvilkar Banglow, Nagala Park,
                                    Kolhapur, Maharashtra (INDIA).
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="26th Tamilnadu State Team Chess Championship – 185220 / TN (S)T / 2018">26th
                                        Tamilnadu State Team Chess Championship - 185220 / TN (S)T / 2018</a></td>
                                <td class="date">21 Jun - 24 Jun</td>
                                <td class="location">Tamil Sangam, Plot no 514, Dr Thangaraj Rd, Tallakulam, Madurai,
                                    Tamil Nadu 625020
                                </td>
                                <td class="url"><a
                                            href=""
                                            title="Email"><i class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Late Ramesh Vinavakrao Kotwal Memorial all India Open Rapid – 187000/MAH(R)/2018">Late
                                        Ramesh Vinavakrao Kotwal Memorial all India Open Rapid - 187000/MAH(R)/2018</a>
                                </td>
                                <td class="date">23 Jun - 24 Jun</td>
                                <td class="location">Shri Bhavbhuti Rang Mandir,Opp Saraswati Girls High School,Poona
                                    Toli,Gondia,MH,441601
                                </td>
                                <td class="url"><a href=" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Chess in Lakecity FIDE Rating Rapid – 182228 / RAJ(R)/ 2018">Chess in
                                        Lakecity FIDE Rating Rapid - 182228 / RAJ(R)/ 2018</a></td>
                                <td class="date">23 Jun - 24 Jun</td>
                                <td class="location">BHANDARI DARSHAK MANDAP,GANDHI GROUND ,Udaipur,RJ</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Maharashtra State Under 15 Boys &amp; Girls – 2018 – 190745 / 190746 / MAH (S) / 2018">Maharashtra
                                        State Under 15 Boys &amp; Girls - 2018 - 190745 / 190746 / MAH (S) / 2018</a>
                                </td>
                                <td class="date">24 Jun - 28 Jun</td>
                                <td class="location">Shivaji Uday Mandal Hall,shukrawar peth, Satara Maharashtra</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Commonwealth Chess Championship 2018  – 181891 to 181898 &amp; 181900 to 181906/ DEL/2018">Commonwealth
                                        Chess Championship 2018 - 181891 to 181898 &amp; 181900 to 181906/ DEL/2018</a>
                                </td>
                                <td class="date">25 Jun - 04 Jul</td>
                                <td class="location">The Leela Ambience Convention Hotel, 1, Central Business District,
                                    Near Yamuna Sports Complex, Maharaja Surajmal Marg, Delhi-32, INDIA
                                </td>
                                <td class="url"><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="State Level Women Chess Tournament ( for Bengal Players) – 190465 / WB(S) / 2018">State
                                        Level Women Chess Tournament ( for Bengal Players) - 190465 / WB(S) / 2018</a>
                                </td>
                                <td class="date">27 Jun - 01 Jul</td>
                                <td class="location">I LEAD, 113, J MATHESWARTOLA ROAD, TOPSIA, KOL-700 046</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark" title="29th Cusat FIDE Rating – 181036 / KER / 2018">29th
                                        Cusat FIDE Rating - 181036 / KER / 2018</a></td>
                                <td class="date">28 Jun - 01 Jul</td>
                                <td class="location">Cusat Campus,Kochi ,KL</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Aditya Birla Memorial 18th Jharkhand State Senior FIDE – 191524 /JHAR(S) /2018">Aditya
                                        Birla Memorial 18th Jharkhand State Senior FIDE - 191524 /JHAR(S) /2018</a></td>
                                <td class="date">28 Jun - 01 Jul</td>
                                <td class="location">Sarala Birla Public School,Tanti Silva Road,JH,8986911555</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Ashoka The Great (Mauryan Dynasty) FIDE rating  – 190847 / TN / 2018">Ashoka
                                        The Great (Mauryan Dynasty) FIDE rating - 190847 / TN / 2018</a></td>
                                <td class="date">05 Jul - 08 Jul</td>
                                <td class="location">Jawaharlal Nehru Stadium,Periamet, Chennai,600003</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Goa State Senior FIDE Rating  – 192934 / GOA (S) / 2018">Goa State
                                        Senior FIDE Rating - 192934 / GOA (S) / 2018</a></td>
                                <td class="date">08 Jul - 12 Jul</td>
                                <td class="location">CADA Hall,Curchorem ,Goa</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st West Bengal State FIDE Rated Under – 17 Open &amp; Girls – 2018 – 190845 / 190846 / WB(S) / 2018">1st
                                        West Bengal State FIDE Rated Under - 17 Open &amp; Girls - 2018 - 190845 /
                                        190846 / WB(S) / 2018</a></td>
                                <td class="date">08 Jul - 11 Jul</td>
                                <td class="location">AYTIJYA BHAWAN, 4 No. Ghumtee, D.B.C. Road, P.O + Dt. Jalpaiguri,
                                    West Bengal - 735101
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark" title="Second Edition FIDE Rated 2018 – 190849 / NE / 2018">Second
                                        Edition FIDE Rated 2018 - 190849 / NE / 2018</a></td>
                                <td class="date">08 Jul - 13 Jul</td>
                                <td class="location">Desbhakta Tarun Ram Phukan Indoor Stadium, RG Baruah Sports
                                    complex, Near Naehru Stadium,Guwahati-781007 (Ulubari)
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="8th St.Joseph’s FIDE Rating Chess Tournament – 191654 / TN / 2018">8th
                                        St.Joseph's FIDE Rating Chess Tournament - 191654 / TN / 2018</a></td>
                                <td class="date">09 Jul - 11 Jul</td>
                                <td class="location">St. Joseph’s college of Engineering,OMR Road , Chennai,TN,600019
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="Surya College FIDE Rated Chess Tournament – 188741 / TN / 2018">Surya
                                        College FIDE Rated Chess Tournament - 188741 / TN / 2018</a></td>
                                <td class="date">12 Jul - 15 Jul</td>
                                <td class="location">Surya Group of Institution, Vikravandi, Villupuram Dt, - 605652
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="4 Queens 1st below 1600 FIDE Chess Tournament – 174084 / KER / 2018">4
                                        Queens 1st below 1600 FIDE Chess Tournament - 174084 / KER / 2018</a></td>
                                <td class="date">13 Jul - 15 Jul</td>
                                <td class="location">Rajiv Gandhi Indoor stadium , Kadavanthara , Ernakulam</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="All India Below 1500 FIDE Rating – 182675 / TEL / 2018">All India
                                        Below 1500 FIDE Rating - 182675 / TEL / 2018</a></td>
                                <td class="date">14 Jul - 16 Jul</td>
                                <td class="location">Delhi Public School Hyderabad,Nacharam,Hyderabad</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                                           rel="bookmark"
                                                           title="National Under – 7 ( Open &amp; Girls ) 2018 – 186619 / 186620 / KAR / 2018">National
                                        Under - 7 ( Open &amp; Girls ) 2018 - 186619 / 186620 / KAR / 2018</a></td>
                                <td class="date">16 Jul - 24 Jul</td>
                                <td class="location">Sri Manjunatha Kalyana Mantapa, BH Road, Vidyanagar, Beside RTO
                                    office, Tumkur - 572103
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="44th National Sub Junior (Under-15 Open) &amp; 35th National Sub Junior (Under-15 Girls) Chess Championships-2018 – 188626 / 188627 / WB / 2018">44th
                                        National Sub Junior (Under-15 Open) &amp; 35th National Sub Junior (Under-15
                                        Girls) Chess Championships-2018 - 188626 / 188627 / WB / 2018</a></td>
                                <td class="date">17 Jul - 25 Jul</td>
                                <td class="location">JIS COLLEGE OF ENGINEERING Block A, Phase III, Kalyani, Nadia -
                                    741235 West Bengal
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="UP State FIDE Rating Chess Championship Under – 25 – 191813 / UP(S) / 2018">UP
                                        State FIDE Rating Chess Championship Under - 25 - 191813 / UP(S) / 2018</a></td>
                                <td class="date">20 Jul - 23 Jul</td>
                                <td class="location">VISHNU BHAGWAN PUBLIC SCHOOL G.T. ROAD KAZIPUR KAUSHAMBI (
                                    KESARWANI DABA)
                                </td>
                                <td class="url"><a
                                            href=""
                                            title="Email"><i class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="National FIDE Rating Open Chess tournament for the visually challenged – 186824 / AICFB / 2018">National
                                        FIDE Rating Open Chess tournament for the visually challenged - 186824 / AICFB /
                                        2018</a></td>
                                <td class="date">26 Jul - 29 Jul</td>
                                <td class="location">Bachhraj Dharmashala, Shastri Chowk, Station Road, Wardha,
                                    Maharashtra
                                </td>
                                <td class="url"><a href="" title="Email"><i class="fa fa-envelope"
                                                                                                  aria-hidden="true"></i>
                                    </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="1st Gnana Vidhya Open FIDE Rated – 170803 / TN / 2018">1st Gnana
                                        Vidhya Open FIDE Rated - 170803 / TN / 2018</a></td>
                                <td class="date">26 Jul - 29 Jul</td>
                                <td class="location">K.Dhanakodi Ammal Thirumana Mahal, 1/121/ Mettukuppam Main Road,
                                    Vanagaram,Chennai - 95
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="32nd National Under – 11 ( Open &amp; Girls ) 2018 – 183472 / 183473 / TN / 2018">32nd
                                        National Under - 11 ( Open &amp; Girls ) 2018 - 183472 / 183473 / TN / 2018</a>
                                </td>
                                <td class="date">28 Jul - 05 Aug</td>
                                <td class="location">Chettinadu Public School,Chettinadu Public School, Managiri,
                                    Karaikudi,Sivagangai District ,TN
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                                           rel="bookmark"
                                                           title="National Cities – 2018  – 189826 / WB / 2018">National
                                        Cities - 2018 - 189826 / WB / 2018</a></td>
                                <td class="date">01 Aug - 05 Aug</td>
                                <td class="location">Siliguri Indoor stadium, Desh Bandhu Para, Siliguri, Dist.
                                    Darjeeling ,Pin 734004
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                            rel="bookmark"
                                            title="11th Modern School FIDE Rating chess tournament for school children – 190868 / TN / 2018">11th
                                        Modern School FIDE Rating chess tournament for school children - 190868 / TN /
                                        2018</a></td>
                                <td class="date">03 Aug - 06 Aug</td>
                                <td class="location">Modern Senior Secondary School,AG's Office Colony,
                                    Nanganallur,Chennai,TN,600061
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a  class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">OFFICIAL EVENTS</a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">OTHER EVENTS</a>
                        </div>
                    </div>
                    <form>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group padd_left_right_none padd_top_20">
                            <select class="form-control" id="sel1">
                                <option value="0">Select a Reg Status</option>
                                <option value="y">Active</option>
                                <option value="p">Processing</option>
                                <option value="n">Inactive</option>
                            </select>
                            <button type="button" class="btn btn-warning">Filter</button>
                            <input class="btn btn-info" onclick="loadpage();" value="Clear Filter" name="clear" class="event_filter_button"
                                   type="reset">
                        </div>
                    </form>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href=""> Chess in Schools</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card holders</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="http://wordpress.lan/twin-international/"> <img src="http://wordpress.lan/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                                    PAYMENTS </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/faq/">FAQ</a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                News
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news1.jpeg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/thumbnail.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Featured Events
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features4.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                    National Under – 11 (Open &amp; Girls)</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features3.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features2.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features1.png"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                            </div>
                        </div>

                        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                            </div>
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>76612</strong>
                                    </div>
                                    <span>Registered Players</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>28574</strong>
                                    </div>
                                    <span>FIDE Rated Players</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>219</strong>
                                    </div>
                                    <span>Tournaments in 2018</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>51</strong>
                                    </div>
                                    <span> Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>101</strong>
                                    </div>
                                    <span> International Masters </span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>7</strong>
                                    </div>
                                    <span> Women Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                AICF Chronicles
                            </div>
                        </div>
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/AICF.jpg">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chee_buttom.jpg">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>