<?php
/*
  Template Name: News
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padd_left_right_none">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announce">News
                        </div>
                        <hr>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/thumbnail.jpg" alt="player">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                   href="">National Under-15: Ajay, Divya crowned champions </a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10 latest-news-excerpt">
                                <p>The 44th National Sub Junior (Under-15 Open) & 35th National Sub Junior (Under-15
                                    Girls) Chess Championships-2018 was held at JIS...</p>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess7.jpg" alt="player">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                   href="">India vs. Spain friendly training camp begins in Kochi</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5 latest-news-excerpt">
                                    <p>Top chess players from India and Spain are testing their mettle on board as part
                                        of the ongoing Toyota Yaris...</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/Champion.jpg" alt="player">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                   href="">Karthikeyan and Tania are Commonwealth Champions</a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5 latest-news-excerpt">
                                <p>Penultimate Round Decisive results in top seven boards in the penultimate round paved
                                    the way for an exciting finish of...</p>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/national.jpg" alt="player">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                   href="">Seven share lead; Meghna stuns Kunte</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20 latest-news-excerpt">
                                    <p>Meghana C.H Report by I.A Gopakumaran Sudhakaran Top three boards in the third
                                        round ended without decisive results paved the...</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href=""> Chess in Schools</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card holders</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <a href="http://wordpress.lan/twin-international/"> <img src="http://wordpress.lan/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                                PAYMENTS </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/faq/">FAQ</a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                            News
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/news.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/news1.jpeg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/thumbnail.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                            Featured Events
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features4.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                National Under – 11 (Open &amp; Girls)</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features3.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features2.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features1.png"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                        </div>
                    </div>

                    <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                        </div>
                        <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                 class="player_img" alt="player">
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>76612</strong>
                                </div>
                                <span>Registered Players</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>28574</strong>
                                </div>
                                <span>FIDE Rated Players</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>219</strong>
                                </div>
                                <span>Tournaments in 2018</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>51</strong>
                                </div>
                                <span> Grand Masters </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                    <strong>101</strong>
                                </div>
                                <span> International Masters </span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                    <strong>7</strong>
                                </div>
                                <span> Women Grand Masters </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                            AICF Chronicles
                        </div>
                    </div>
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/AICF.jpg">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                            Mate in two moves
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/chee_buttom.jpg">
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>