<?php
/*
  Template Name: Playersearch
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none ">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_left_none announce">
                                Top 100 Players
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_left_none announce">
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12  padd_left_none announce">
                                <a class="next" href="http://wordpress.lan/playersecond/">Next >></a>
                            </div>
                            <hr>
                        </div>


                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover">
                            <tbody>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Titles</th>
                                <th>State</th>
                                <th>Ratings</th>
                                <th>Reg Status</th>
                                <th>Gender</th>
                            </tr>
                            <tr>
                                <td class="player-table-value">1</td>
                                <td><a class="player-table-values" href="">Anand,
                                        Viswanathan</a></td>

                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2768</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">2</td>
                                <td><a class="player-table-values" href="">Harikrishna,
                                        P.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2734</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">3</td>
                                <td><a class="player-table-values" href="">Vidit,
                                        Santosh Gujrathi</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2718</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">4</td>
                                <td><a class="player-table-values" href="">Adhiban,
                                        B.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2671</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">5</td>
                                <td><a class="player-table-values" href="">Sasikiran,
                                        Krishnan</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2666</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">6</td>
                                <td><a class="player-table-values" href="">Sethuraman,
                                        S.P.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2657</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">7</td>
                                <td><a class="player-table-values" href="">Negi,
                                        Parimarjan</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2656</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">8</td>
                                <td><a class="player-table-values" href="">Ganguly,
                                        Surya Shekhar</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2652</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">9</td>
                                <td><a class="player-table-values" href="">Gupta,
                                        Abhijeet</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2614</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">10</td>
                                <td><a class="player-table-values" href="">Karthikeyan,
                                        Murali</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2609</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">11</td>
                                <td><a class="player-table-values" href="">Gopal
                                        G.N.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Kerala</td>
                                <td class="player-table-value">2593</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">12</td>
                                <td><a class="player-table-values" href="">Aravindh,Chithambaram
                                        VR.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2580</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">13</td>
                                <td><a class="player-table-values" href="">Narayanan.S.L</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Kerala</td>
                                <td class="player-table-value">2574</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">14</td>
                                <td><a class="player-table-values" href="">Sengupta,
                                        Deep</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2565</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">15</td>
                                <td><a class="player-table-values" href="">Koneru,
                                        Humpy</a></td>
                                <td class="player-table-value">GM,&nbsp;WGM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2557</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">16</td>
                                <td><a class="player-table-values" href="">Vaibhav,
                                        Suri</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2556</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">17</td>
                                <td><a class="player-table-values" href="">Nihal
                                        Sarin</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Kerala</td>
                                <td class="player-table-value">2551</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">18</td>
                                <td><a class="player-table-values" href="">Narayanan,
                                        Srinath</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2549</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">19</td>
                                <td><a class="player-table-values" href="">Chanda,
                                        Sandipan</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2548</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">20</td>
                                <td><a class="player-table-values" href="">Ghosh,
                                        Diptayan</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2543</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">21</td>
                                <td><a class="player-table-values" href="">Kannappan
                                        Priyadharshan</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2540</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">22</td>
                                <td><a class="player-table-values" href="">Aryan
                                        Chopra</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2536</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">23</td>
                                <td><a class="player-table-values" href="">Lalith Babu M
                                        R</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2529</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">24</td>
                                <td><a class="player-table-values" href="">Debashis,
                                        Das</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2529</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">25</td>
                                <td><a class="player-table-values" href="">Praggnanandhaa
                                        R</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2529</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">26</td>
                                <td><a class="player-table-values" href="">Shyam, Sundar
                                        M.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2527</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">27</td>
                                <td><a class="player-table-values" href="">Vishnu
                                        Prasanna. V</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2525</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">28</td>
                                <td><a class="player-table-values" href="">Deepan
                                        Chakkravarthy J.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2519</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">29</td>
                                <td><a class="player-table-values" href="">Karthik
                                        Venkataraman</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2514</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">30</td>
                                <td><a class="player-table-values" href="">Puranik,
                                        Abhimanyu</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2507</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">31</td>
                                <td><a class="player-table-values" href="">Grover,
                                        Sahaj</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2507</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">32</td>
                                <td><a class="player-table-values" href="">Arun Prasad,
                                        S.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2501</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">33</td>
                                <td><a class="player-table-values" href="">Saptarshi,
                                        Roy</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2500</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">34</td>
                                <td><a class="player-table-values" href="">Erigaisi
                                        Arjun</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Telangana</td>
                                <td class="player-table-value">2499</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">35</td>
                                <td><a class="player-table-values" href="">Venkatesh,
                                        M.R.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2499</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">36</td>
                                <td><a class="player-table-values" href="">Thejkumar, M.
                                        S.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Karnataka</td>
                                <td class="player-table-value">2495</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">37</td>
                                <td><a class="player-table-values" href="">Swapnil, S.
                                        Dhopade</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2495</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">38</td>
                                <td><a class="player-table-values" href="">Kunte,
                                        Abhijit</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2494</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">39</td>
                                <td><a class="player-table-values" href="">Harika,
                                        Dronavalli</a></td>
                                <td class="player-table-value">GM,&nbsp;WGM</td>
                                <td class="player-table-value">Telangana</td>
                                <td class="player-table-value">2494</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">40</td>
                                <td><a class="player-table-values" href="">Ashwin,
                                        Jayaram</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2493</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">41</td>
                                <td><a class="player-table-values" href="">Gagare,
                                        Shardul</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2490</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">42</td>
                                <td><a class="player-table-values" href="">Panchanathan,
                                        Magesh Chandran</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2486</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">43</td>
                                <td><a class="player-table-values" href="">Anurag,
                                        Mhamal</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Goa</td>
                                <td class="player-table-value">2484</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">44</td>
                                <td><a class="player-table-values" href="">Stany,
                                        G.A.</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Karnataka</td>
                                <td class="player-table-value">2483</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">45</td>
                                <td><a class="player-table-values" href="">Akshayraj,
                                        Kore</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2482</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">46</td>
                                <td><a class="player-table-values" href="">Ramesh R
                                        B</a></td>
                                <td class="player-table-value">GM,&nbsp;FT</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2472</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">47</td>
                                <td><a class="player-table-values" href="">Visakh N
                                        R</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2471</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">48</td>
                                <td><a class="player-table-values" href="">Vardan
                                        Nagpal</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2468</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">49</td>
                                <td><a class="player-table-values" href="">Ankit, R.
                                        Rajpara</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Gujarat</td>
                                <td class="player-table-value">2465</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">50</td>
                                <td><a class="player-table-values" href="">Harsha
                                        Bharathakoti</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Telangana</td>
                                <td class="player-table-value">2465</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">51</td>
                                <td><a class="player-table-values" href="">Vignesh N
                                        R</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2464</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">52</td>
                                <td><a class="player-table-values" href="">Swayams,
                                        Mishra</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2463</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">53</td>
                                <td><a class="player-table-values" href="">Prasanna
                                        Raghuram Rao</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2462</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">54</td>
                                <td><a class="player-table-values" href="">Iniyan,
                                        P</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2451</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">55</td>
                                <td><a class="player-table-values" href="">Laxman,
                                        R.R.</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2449</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">56</td>
                                <td><a class="player-table-values" href="">Barua,
                                        Dibyendu</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2443</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">57</td>
                                <td><a class="player-table-values" href="">Rohit,
                                        Gogineni</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2442</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">58</td>
                                <td><a class="player-table-values" href="">Akash Pc,
                                        Iyer</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2442</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">59</td>
                                <td><a class="player-table-values" href="">Raja
                                        Harshit</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2442</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">60</td>
                                <td><a class="player-table-values" href="">Mohammad
                                        Nubairshah Shaikh</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2441</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">61</td>
                                <td><a class="player-table-values" href="">Bakre,
                                        Tejas</a></td>
                                <td class="player-table-value">GM,&nbsp;FT</td>
                                <td class="player-table-value">Gujarat</td>
                                <td class="player-table-value">2439</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">62</td>
                                <td><a class="player-table-values" href="">Das,
                                        Arghyadip</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2436</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">63</td>
                                <td><a class="player-table-values" href="">Nitin, S.</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2436</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">64</td>
                                <td><a class="player-table-values" href="">Prithu
                                        Gupta</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2436</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">65</td>
                                <td><a class="player-table-values" href="">Gukesh D</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2433</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">66</td>
                                <td><a class="player-table-values" href="">Karthikeyan,
                                        P.</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2430</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">67</td>
                                <td><a class="player-table-values" href="">Neelotpal,
                                        Das</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2426</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">68</td>
                                <td><a class="player-table-values" href="">Sundararajan,
                                        Kidambi</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2425</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">69</td>
                                <td><a class="player-table-values" href="">Das,
                                        Sayantan</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2425</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">70</td>
                                <td><a class="player-table-values" href="">Chakravarthi
                                        Reddy, M</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2421</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">71</td>
                                <td><a class="player-table-values" href="">Himanshu,
                                        Sharma</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Haryana</td>
                                <td class="player-table-value">2420</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">72</td>
                                <td><a class="player-table-values" href="">Raghunandan,
                                        Kaumandur Srihari</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Karnataka</td>
                                <td class="player-table-value">2417</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">73</td>
                                <td><a class="player-table-values" href="">Parikh,
                                        Valay</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2411</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">74</td>
                                <td><a class="player-table-values" href="">Satyapragyan,
                                        Swayangsu</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2411</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">75</td>
                                <td><a class="player-table-values" href="">Girish, A.
                                        Koushik</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Karnataka</td>
                                <td class="player-table-value">2411</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">76</td>
                                <td><a class="player-table-values" href="">Fenil,
                                        Shah</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Gujarat</td>
                                <td class="player-table-value">2411</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">77</td>
                                <td><a class="player-table-values" href="">Sriram,
                                        Jha</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2409</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">78</td>
                                <td><a class="player-table-values" href="">Sadhwani,
                                        Raunak</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2409</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">79</td>
                                <td><a class="player-table-values" href="">Sagar,
                                        Shah</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2407</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">80</td>
                                <td><a class="player-table-values" href="">Thipsay,
                                        Praveen M</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2405</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">81</td>
                                <td><a class="player-table-values" href="">Viani Antonio
                                        Dcunha</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Karnataka</td>
                                <td class="player-table-value">2404</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">82</td>
                                <td><a class="player-table-values" href="">Prince,
                                        Bajaj</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2404</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">83</td>
                                <td><a class="player-table-values" href="">Krishna C R
                                        G</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2399</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">84</td>
                                <td><a class="player-table-values" href="">Audi
                                        Ameya</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Goa</td>
                                <td class="player-table-value">2399</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">85</td>
                                <td><a class="player-table-values" href="">Bitan,
                                        Banerjee</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2398</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">86</td>
                                <td><a class="player-table-values" href="">Udeshi,
                                        Aditya</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2398</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">87</td>
                                <td><a class="player-table-values" href="">Rahul
                                        Srivatshav P</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2397</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">88</td>
                                <td><a class="player-table-values" href="">Hemant,
                                        Sharma (del)</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2393</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">89</td>
                                <td><a class="player-table-values" href="">Tania,
                                        Sachdev</a></td>
                                <td class="player-table-value">IM,&nbsp;WGM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2393</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">90</td>
                                <td><a class="player-table-values" href="">Umakanth,
                                        Sharma</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2391</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">91</td>
                                <td><a class="player-table-values" href="">Akash G</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2391</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">92</td>
                                <td><a class="player-table-values" href="">Karavade,
                                        Eesha</a></td>
                                <td class="player-table-value">IM,&nbsp;WGM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2390</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">93</td>
                                <td><a class="player-table-values" href="">Krishna Teja,
                                        N</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2389</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">94</td>
                                <td><a class="player-table-values" href="">Poobesh
                                        Anand, S.</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2389</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">95</td>
                                <td><a class="player-table-values" href="">Sai Krishna G
                                        V</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2388</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">96</td>
                                <td><a class="player-table-values" href="">Akshat,
                                        Khamparia</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Madhya Pradesh</td>
                                <td class="player-table-value">2388</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">97</td>
                                <td><a class="player-table-values" href="">Rohan,
                                        Ahuja</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Goa</td>
                                <td class="player-table-value">2387</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">98</td>
                                <td><a class="player-table-values" href="">Rakesh Kumar
                                        Jena</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2385</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">99</td>
                                <td><a class="player-table-values" href="">Gusain,
                                        Himal</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Chandigarh</td>
                                <td class="player-table-value">2384</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">100</td>
                                <td><a class="player-table-values" href="">Roktim,
                                        Bandyopadhyay</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2383</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            </tbody>
                        </table>



                        <?php
                        $args = array(
                            'post_type' => 'players',
                            'posts_per_page' => 8
                            // Several more arguments could go here. Last one without a comma.
                        );

                        // Query the posts:
                        $players_query = new WP_Query($args);

                        // Loop through the obituaries:
                        while ($players_query->have_posts()) : $players_query->the_post();
                            // Echo some markup
                            echo '<p>';
                            // As with regular posts, you can use all normal display functions, such as
                            the_title();
                            // Within the loop, you can access custom fields like so:

                            echo '</p>'; // Markup closing tags.
                            echo esc_attr(get_post_meta(get_the_ID(), 'player_name', true));
                            echo esc_attr(get_post_meta(get_the_ID(), 'player_title', true));
                            echo esc_attr(get_post_meta(get_the_ID(), 'player_state', true));
                            echo esc_attr(get_post_meta(get_the_ID(), 'player_ratings', true));
                            echo esc_attr(get_post_meta(get_the_ID(), 'player_regstatus', true));
                            echo esc_attr(get_post_meta(get_the_ID(), 'player_gender', true));
                        endwhile;

                        // Reset Post Data
                        wp_reset_postdata();

                        ?>





                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover">
                            <tbody>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Titles</th>
                                <th>State</th>
                                <th>Ratings</th>
                                <th>Reg Status</th>
                                <th>Gender</th>
                            </tr>
                            <tr>
                                <td class="player-table-value">1</td>
                                <td><a class="player-table-values" href="">Anand,
                                        Viswanathan</a></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                    <form>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group padd_top_20">
                            <input type="text" class="form-control" placeholder="Player Name / FIDE ID / AICF ID" name="q" id="usr">

                            <select class="form-control" id="sel1">
                                <option value="0">Select a Genter</option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>

                            <select class="form-control" id="sel1">
                                <option>Select a State</option>
                                <option value="0">Select a State</option>
                                <option value="1">Delhi</option>
                                <option value="2">Tamil Nadu</option>
                                <option value="3">Andhra Pradesh</option>
                                <option value="4">Arunachal Pradesh</option>
                                <option value="5">Assam</option>
                                <option value="6">Bihar</option>
                                <option value="7">Chhattisgarh</option>
                                <option value="8">Goa</option>
                                <option value="9">Gujarat</option>
                                <option value="10">Himachal Pradesh</option>
                                <option value="11">Haryana</option>
                                <option value="12">Jammu and Kashmir</option>
                                <option value="13">Jharkhand</option>
                                <option value="14">Karnataka</option>
                                <option value="15">Kerala</option>
                                <option value="16">Madhya Pradesh</option>
                                <option value="17">Maharashtra</option>
                                <option value="18">Manipur</option>
                                <option value="19">Meghalaya</option>
                                <option value="20">Mizoram</option>
                                <option value="21">Orissa</option>
                                <option value="22">Nagaland</option>
                                <option value="23">Punjab</option>
                                <option value="24">Rajasthan</option>
                                <option value="25">Sikkim</option>
                                <option value="26">Tripura</option>
                                <option value="27">Uttrakhand</option>
                                <option value="28">Uttar Pradesh</option>
                                <option value="29">West Bengal</option>
                                <option value="30">Andaman and Nicobar Islands</option>
                                <option value="31">Chandigarh</option>
                                <option value="32">Dadar and Nagar Haveli</option>
                                <option value="33">Lakshadeep</option>
                                <option value="34">Pondicherry</option>
                                <option value="35">Daman and Diu</option>
                                <option value="36">Telangana</option>
                                <option value="37">Others</option>
                            </select>

                            <select class="form-control" id="sel1">
                                <option value="0">Select a Type</option>
                                <option value="p">Player</option>
                                <option value="a">Arbiter</option>
                                <option value="c">Coach</option>
                            </select>
                            <select class="form-control" id="sel1">
                                <option value="0">Select a Title</option>
                                <option value="GM">GM</option>
                                <option value="WGM">WGM</option>
                                <option value="FT">FT</option>
                                <option value="IM">IM</option>
                                <option value="FST">FST</option>
                                <option value="FA">FA</option>
                                <option value="IA">IA</option>
                                <option value="DI">DI</option>
                                <option value="FM">FM</option>
                                <option value="FI">FI</option>
                                <option value="NI">NI</option>
                                <option value="CM">CM</option>
                                <option value="WIM">WIM</option>
                                <option value="WFM">WFM</option>
                                <option value="WCM">WCM</option>
                            </select>

                            <select class="form-control" id="sel1">
                                <option value="0">Select a Reg Status</option>
                                <option value="y">Active</option>
                                <option value="p">Processing</option>
                                <option value="n">Inactive</option>
                            </select>
                            <button type="button" class="btn">Filter</button>
                            <input class="btns" onclick="loadpage();" value="Clear Filter" name="clear" class="event_filter_button" type="reset">
                        </div>
                    </form>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href=""> Chess in Schools</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card holders</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="http://wordpress.lan/twin-international/"> <img src="http://wordpress.lan/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                                    PAYMENTS </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/faq/">FAQ</a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                News
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news1.jpeg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/thumbnail.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Featured Events
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features4.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                    National Under – 11 (Open &amp; Girls)</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features3.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features2.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features1.png"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                            </div>
                        </div>

                        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                            </div>
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>76612</strong>
                                    </div>
                                    <span>Registered Players</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>28574</strong>
                                    </div>
                                    <span>FIDE Rated Players</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>219</strong>
                                    </div>
                                    <span>Tournaments in 2018</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>51</strong>
                                    </div>
                                    <span> Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>101</strong>
                                    </div>
                                    <span> International Masters </span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>7</strong>
                                    </div>
                                    <span> Women Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                AICF Chronicles
                            </div>
                        </div>
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/AICF.jpg">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chee_buttom.jpg">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>